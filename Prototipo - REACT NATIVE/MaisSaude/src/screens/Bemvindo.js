import React, { Component } from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function BemVindo(props) {
  return (
    <View style={styles.container}>
      <View style={styles.groupStack}>
        <View style={styles.group}>
          <Text style={styles.exercicio1}>Bem-vindo Sr. Valente</Text>
          <Image
            source={require("../assets/images/photo.png")}
            resizeMode="contain"
            style={styles.img}
          ></Image>
        </View>
        <TouchableOpacity style={styles.button}
        onPress={() => props.navigation.navigate("IniciarSessao")}>
          <View style={styles.ellipseStack}>
            <Svg viewBox="0 0 40 40" style={styles.ellipse}>
              <Ellipse
                stroke="rgba(230, 230, 230,1)"
                strokeWidth={0}
                fill="rgba(230, 230, 230,1)"
                cx={20}
                cy={20}
                rx={20}
                ry={20}
              ></Ellipse>
            </Svg>
            <Text style={styles.text}>🔃</Text>
          </View>
        </TouchableOpacity>
      </View>
      <Image
        source={require("../assets/images/image_jnaa..png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1a5b92"
  },
  group: {
    top: 0,
    left: 0,
    width: 375,
    height: 201,
    position: "absolute",
    alignSelf:"center"
  },
  exercicio1: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    alignSelf: "center"
  },
  img: {
    width: 150,
    height: 150,
    marginTop: 10,
    marginLeft: 114
  },
  button: {
    top: 174,
    left: 244,
    width: 40,
    height: 40,
    position: "absolute",
    borderRadius: 100
  },
  ellipse: {
    top: 12,
    left: 12,
    position: "absolute",
    height: 0,
    width: 0,
    opacity: 0
  },
  text: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 35,
    left: 0,
    top: 0
  },
  ellipseStack: {
    width: 65,
    height: 63,
    marginTop: -12,
    marginLeft: -12
  },
  groupStack: {
    width: 375,
    height: 204,
    marginTop: 280,
    alignSelf: "center"
  },
  image: {
    width: 50,
    height: 50,
    marginTop: 226,
    alignSelf: "center"
  }
});

export default BemVindo;

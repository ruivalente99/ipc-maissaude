import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function NotExer(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect}>
        <View style={styles.group}>
          <Text style={styles.exercicio}>Exercício</Text>
          <Text style={styles.iDExercico}>Correr</Text>
          <Text style={styles.tempo}>10 minutos</Text>
          <Svg viewBox="0 0 170.14 169.61" style={styles.fotoExercicio}>
            <Ellipse
              strokeWidth={0}
              fill="rgba(122,121,121,1)"
              cx={85}
              cy={85}
              rx={85}
              ry={85}
            ></Ellipse>
          </Svg>
          <View style={styles.buttonConfirmRow}>
            <View style={styles.buttonConfirm}>
              <TouchableOpacity
                onPress={() => props.navigation.goBack()}
                style={styles.button}
              >
                <Text style={styles.jaFiz}>Já fiz</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonConfirmFiller}></View>
            <View style={styles.buttonAdiar}>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("NotExer")}
                style={styles.button2}
              >
                <Text style={styles.adiar}>+10min</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  rect: {
    height: 545,
    backgroundColor: "#E6E6E6",
    borderTopEndRadius: 55,
    borderTopLeftRadius: 55,
    marginTop: 314
  },
  group: {
    height: 170,
    marginTop: 198
  },
  exercicio: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 45,
    marginTop: -158,
    alignSelf: "center"
  },
  iDExercico: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 45,
    marginTop: 10,
    alignSelf: "center"
  },
  tempo: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    alignSelf: "center"
  },
  fotoExercicio: {
    width: 170,
    height: 170,
    marginTop: 11,
    alignSelf: "center"
  },
  buttonConfirm: {
    width: 138,
    height: 41
  },
  button: {
    width: 142,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderRadius: 55,
    marginTop: -21,
    marginLeft: -73
  },
  jaFiz: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "justify",
    marginTop: 21,
    marginLeft: 29
  },
  buttonConfirmFiller: {
    flex: 1,
    flexDirection: "row"
  },
  buttonAdiar: {
    width: 138,
    height: 41
  },
  button2: {
    width: 142,
    height: 84,
    backgroundColor: "rgba(26,91,146,1)",
    borderRadius: 55,
    marginTop: -21,
    marginLeft: -73
  },
  adiar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "justify",
    marginTop: 21,
    marginLeft: 12
  },
  buttonConfirmRow: {
    height: 41,
    flexDirection: "row",
    marginTop: 38,
    marginLeft: 89,
    marginRight: -52
  }
});

export default NotExer;

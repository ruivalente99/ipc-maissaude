import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import { Center } from "@builderx/utils";
import Svg, { Ellipse } from "react-native-svg";

function IniciarSessao(props) {
  return (
    <View style={styles.container}>
      <View style={styles.group}>
        <Text style={styles.text}>Iniciar{"\n"}sessão</Text>
        <TouchableOpacity style={styles.button4}>
          <Text style={styles.continuarSemConta}>Continuar sem conta</Text>
        </TouchableOpacity>
        <View style={styles.imageStack}>
          <Center horizontal>
            <Image
              source={require("../assets/images/image_YWE4..png")}
              resizeMode="contain"
              style={styles.image}
            ></Image>
          </Center>
          <View style={styles.rect}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.button}
            >
              <View style={styles.fotoBtnRow}>
                <Svg viewBox="0 0 60 59.81" style={styles.fotoBtn}>
                  <Ellipse
                    strokeWidth={0}
                    fill="rgba(122,121,121,1)"
                    cx={30}
                    cy={30}
                    rx={30}
                    ry={30}
                  ></Ellipse>
                </Svg>
                <Text style={styles.google}>Google</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.buttonApple}>
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={styles.button2}
          >
            <View style={styles.fotoBtn1Row}>
              <Svg viewBox="0 0 60 59.81" style={styles.fotoBtn1}>
                <Ellipse
                  strokeWidth={0}
                  fill="rgba(122,121,121,1)"
                  cx={30}
                  cy={30}
                  rx={30}
                  ry={30}
                ></Ellipse>
              </Svg>
              <Text style={styles.apple}>Apple</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonEmail}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("LoginEd")}
            style={styles.button3}
          >
            <View style={styles.fotoBtn2Row}>
              <Svg viewBox="0 0 60 59.81" style={styles.fotoBtn2}>
                <Ellipse
                  strokeWidth={0}
                  fill="rgba(122,121,121,1)"
                  cx={30}
                  cy={30}
                  rx={30}
                  ry={30}
                ></Ellipse>
              </Svg>
              <Text style={styles.email}>Email</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  group: {
    height: 80,
    marginTop: 685
  },
  text: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 50,
    textAlign: "center",
    marginTop: -597,
    alignSelf: "center"
  },
  button4: {
    height: 28,
    marginTop: 405,
  },
  continuarSemConta: {
    fontFamily: "roboto-regular",
    color: "rgba(183,181,181,1)",
    fontSize: 24,
    alignSelf: "center"
  },
  image: {
    position: "absolute",
    top: 0,
    height: 80,
    width: 80
  },
  rect: {
    top: 19,
    height: 41,
    position: "absolute",
    left: 0,
    right: 0
  },
  button: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 55,
    flexDirection: "row",
    marginTop: -416,
    alignSelf: "center"
  },
  fotoBtn: {
    width: 60,
    height: 60
  },
  google: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 24,
    textAlign: "justify",
    marginLeft: 28,
    marginTop: 16
  },
  fotoBtnRow: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 100,
    marginLeft: 14,
    marginTop: 11
  },
  imageStack: {
    height: 80,
    marginTop: 44
  },
  buttonApple: {
    width: 138,
    height: 41,
    marginTop: 48,
    alignSelf: "center"
  },
  button2: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 55,
    flexDirection: "row",
    marginTop: -416,
    marginLeft: -73
  },
  fotoBtn1: {
    width: 60,
    height: 60
  },
  apple: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 24,
    textAlign: "justify",
    marginLeft: 35,
    marginTop: 17
  },
  fotoBtn1Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 111,
    marginLeft: 16,
    marginTop: 11
  },
  buttonEmail: {
    width: 138,
    height: 41,
    marginTop: 65,
    alignSelf: "center"
  },
  button3: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 55,
    flexDirection: "row",
    marginTop: -416,
    marginLeft: -73
  },
  fotoBtn2: {
    width: 60,
    height: 60
  },
  email: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 24,
    textAlign: "justify",
    marginLeft: 36,
    marginTop: 17
  },
  fotoBtn2Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 112,
    marginLeft: 16,
    marginTop: 11
  }
});

export default IniciarSessao;

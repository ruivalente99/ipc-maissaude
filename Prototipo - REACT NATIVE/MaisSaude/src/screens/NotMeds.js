import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function NotMeds(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect}>
        <View style={styles.group}>
          <Text style={styles.hora}>11:40</Text>
          <Text style={styles.iDMeds}>XANAX</Text>
          <Text style={styles.quantidade}>1mg</Text>
          <Svg viewBox="0 0 170.14 169.61" style={styles.fotoMeds}>
            <Ellipse
              strokeWidth={0}
              fill="rgba(122,121,121,1)"
              cx={85}
              cy={85}
              rx={85}
              ry={85}
            ></Ellipse>
          </Svg>
          <View style={styles.buttonToma}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.button}
            >
              <Text style={styles.jaTomei}>Já Tomei</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  rect: {
    height: 560,
    backgroundColor: "#E6E6E6",
    borderRadius: 55,
    marginTop: 317
  },
  group: {
    height: 170,
    marginTop: 185
  },
  hora: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 45,
    marginTop: -158,
    alignSelf: "center"
  },
  iDMeds: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 45,
    marginTop: 9,
    alignSelf: "center"
  },
  quantidade: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    alignSelf: "center"
  },
  fotoMeds: {
    width: 170,
    height: 170,
    marginTop: 12,
    alignSelf: "center"
  },
  buttonToma: {
    width: 138,
    height: 41,
    marginTop: 35,
    alignSelf: "center"
  },
  button: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderRadius: 55,
    marginTop: -16,
    marginLeft: -78
  },
  jaTomei: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "justify",
    marginTop: 22,
    marginLeft: 78
  }
});

export default NotMeds;

import React, {  } from "react";
import MyDatePicker from "../components/myDatePicker";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity} from "react-native";

function Consult(props) {
  return (
    <View style={styles.container}>
      <View style={styles.topBtnStack}>
        <View style={styles.topBtn}>
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={styles.button1}
          >
            <Image
              source={require("../assets/images/2961062-2001.png")}
              resizeMode="contain"
              style={styles.image1}
            ></Image>
          </TouchableOpacity>
          <View style={styles.button1Filler}></View>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Main")}
            style={styles.button2}
          >
            <Image
              source={require("../assets/images/unknown.png")}
              resizeMode="contain"
              style={styles.image2}
            ></Image>
          </TouchableOpacity>
        </View>
        <View style={styles.items}>
          <View style={styles.consultaColumn}>
            <Text style={styles.consulta}>Consulta</Text>
            <View style={styles.calendario1} >
            <MyDatePicker/>
              <View style={styles.back1}></View>
            </View>
            <View style={styles.group}>
              <View style={styles.rect3}>
                
              </View>
              <View style={styles.nome2}>
                <TextInput
                  placeholder="Descrição"
                  keyboardType="default"
                  maxLength={8}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  placeholderTextColor="rgba(230, 230, 230,1)"
                  style={styles.textInput2}
                ></TextInput>
                <View style={styles.rect2}></View>
              </View>
            </View>
          </View>
          <View style={styles.consultaColumnFiller}></View>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Main")}
            style={styles.buttonMeds1}
          >
            <View style={styles.btmBack1}>
              <Text style={styles.agendar}>Agendar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1a5b92"
  },
  topBtn: {
    width: '90%',
    height: 50,
    position: "absolute",
    flexDirection: "row",
    alignSelf: "center"
  },
  button1: {
    width: 50,
    height: 50
  },
  image1: {
    width: 50,
    height: 50
  },
  button1Filler: {
    flex: 1,
    flexDirection: "row"
  },
  button2: {
    width: 50,
    height: 50
  },
  image2: {
    width: 50,
    height: 50
  },
  items: {
    height: 644,
    position: "absolute",
    left: 0,
    right: 0,
    top: 44
  },
  consulta: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center"
  },
  calendario1: {
    width:'80%',
    top: 15,
    alignSelf: "center",
  },

  group: {
    height: 157,
    marginTop: 20
  },
  rect3: {
    width: 220,
    backgroundColor: "#E6E6E6",

  },
  nome2: {
    height: 40,
    marginTop: 31
  },
  textInput2: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect2: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  consultaColumn: {},
  consultaColumnFiller: {
    flex: 1
  },
  buttonMeds1: {
    width: 284,
    height: 84,
    alignSelf: "center"
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  btmBack2: {
    marginTop:10,
    width: 165,
    height: 84,
    backgroundColor: "rgba(0,0,0,0.2)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55,
    alignSelf: "center"
  },
  agendar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 26
  },
  agendar2: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    textAlign: "center",
  },
  topBtnStack: {
    height: 688,
    marginTop: 40
  }
});

export default Consult;

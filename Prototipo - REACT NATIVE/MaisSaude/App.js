import React, { useState } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import AppLoading from "expo-app-loading";

import * as Font from "expo-font";
//Rui
import Main from "./src/screens/Main";
import ViewMeds from "./src/screens/ViewMeds";
import ViewExs from "./src/screens/ViewExs";
import ViewConsult from "./src/screens/ViewConsult";
import Qr from "./src/screens/Qr";
import DoctorMain from "./src/screens/DoctorMain";
import DocControl from "./src/screens/DocControl";
import ReadQr from "./src/screens/ReadQr";
//Chico
import AdminHomepage from "./src/screens/AdminHomePage";
import BloquearMed from "./src/screens/BloquearMed";
import Def from "./src/screens/Def";
import DefConta from "./src/screens/DefContaInformacao";
import DefLinguagem from "./src/screens/DefLinguagem";
import Login from "./src/screens/Login";
import AprovarMed from "./src/screens/AprovarMed";
//Ed
import NotConsultDa from "./src/screens/NotConsultDa";
import NotConsultH from "./src/screens/NotConsultH";
import NotExer from "./src/screens/NotExer";
import NotMeds from "./src/screens/NotMeds";
import Bemvindo from "./src/screens/Bemvindo";
import LoginEd from "./src/screens/LoginEd";
import IniciarSessao from "./src/screens/IniciarSessao";
//Louro
import Logo from "./src/screens/Logo";
import EditConsult from "./src/screens/EditConsult";
import EditExer from "./src/screens/EditExer";
import EditMeds from "./src/screens/EditMeds";
import FormEx from "./src/screens/FormEx";
import FormMed from "./src/screens/FormMed";
import Consult from "./src/screens/Consult";
const DrawerNavigation = createDrawerNavigator({
  // //Rui
    Main: Main,
  //   ViewMeds: ViewMeds,
  //   ViewExs: ViewExs,
  //   ViewConsult: ViewConsult,
  //   Qr: Qr,
    DoctorMain: DoctorMain,
  //   ReadQr: ReadQr,
  //   DocControl: DocControl,
  //   Def: Def,
  // // Pinto
    AdminHomepage: AdminHomepage,
    // Def: Def,
    // DefConta: DefConta,
    // DefLinguagem: DefLinguagem,
    // AprovarMed: AprovarMed,
    // BloquearMed: BloquearMed,
    // Login: Login,
  //Louro
    // Logo: Logo,
    //Consult: Consult,
    //EditMeds: EditMeds,
    //EditExer: EditExer,
    //EditConsult: EditConsult,
    //FormEx: FormEx,
    //FormMed: FormMed,
  //Ed
    //Bemvindo: Bemvindo,
    //IniciarSessao: IniciarSessao,
    //LoginEd: LoginEd,
    //NotConsultDa: NotConsultDa,
    //NotConsultH: NotConsultH,
    //NotExer: NotExer,
    //NotMeds: NotMeds
});

const StackNavigation = createStackNavigator(
  {
    DrawerNavigation: {
      screen: DrawerNavigation
    },
   // //Rui
    Main: Main,
    ViewMeds: ViewMeds,
    ViewExs: ViewExs,
    ViewConsult: ViewConsult,
    Qr: Qr,
    DoctorMain: DoctorMain,
    ReadQr: ReadQr,
    DocControl: DocControl,
    Def: Def,
  // Pinto
    AdminHomepage: AdminHomepage,
    Def: Def,
    DefConta: DefConta,
    DefLinguagem: DefLinguagem,
    AprovarMed: AprovarMed,
    BloquearMed: BloquearMed,
    Login: Login,
  //Louro
    Logo: Logo,
    Consult: Consult,
    EditMeds: EditMeds,
    EditExer: EditExer,
    EditConsult: EditConsult,
    FormEx: FormEx,
    FormMed: FormMed,
  //Ed
    Bemvindo: Bemvindo,
    IniciarSessao: IniciarSessao,
    LoginEd: LoginEd,
    NotConsultDa: NotConsultDa,
    NotConsultH: NotConsultH,
    NotExer: NotExer,
    NotMeds: NotMeds
  },
  {
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(StackNavigation);

function App() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return isLoadingComplete ? <AppContainer /> : <AppLoading />;
  }
}
async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf")
    })
  ]);
}
function handleLoadingError(error) {
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

export default App;

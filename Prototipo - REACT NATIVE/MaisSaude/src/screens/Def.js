import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground
} from "react-native";

function Def(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Text style={styles.definicoes}>Definições</Text>
      <View style={styles.menu}>
        <View style={styles.background}>
          <View style={styles.buttonsMain1}>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("DefLinguagem")}
              style={styles.buttonMeds1}
            >
              <View style={styles.btmBack1}>
                <Text style={styles.btmText1}>🌍 Linguagem</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("DefConta")}
              style={styles.buttonConsult1}
            >
              <View style={styles.btcBack1}>
                <Text style={styles.btcText1}>👨 Conta</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Bemvindo")}
              style={styles.buttonEx1}
            >
              <View style={styles.btexBack1}>
                <Text style={styles.btexText1}>🚪Terminar Sessão</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.button1Stack}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button1}
        >
          <Image
            source={require("../assets/images/2961062-2001.png")}
            resizeMode="contain"
            style={styles.image1}
          ></Image>
        </TouchableOpacity>
        <View style={styles.profile1}>
          <ImageBackground
            source={require("../assets/images/image_XKVx..png")}
            resizeMode="contain"
            style={styles.profilePic1}
            imageStyle={styles.profilePic1_imageStyle}
          >
            
          </ImageBackground>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  definicoes: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 240
  },
  menu: {
    height: 492,
    marginTop: 39
  },
  background: {
    height: 492,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  buttonsMain1: {
    height: 273,
    width: 375,
    marginTop: 110,
    alignSelf: "center"
  },
  buttonMeds1: {
    width: 284,
    height: 84,
    justifyContent: "center",
    alignSelf: "center"
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55,
    justifyContent: "center",
    alignSelf: "center"
  },
  btmText1: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    textAlign: "center"
  },
  buttonConsult1: {
    width: 284,
    height: 84,
    marginTop: 10,
    alignSelf: "center"
  },
  btcBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  btcText1: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    textAlign: "center",
    marginTop: 20
  },
  buttonEx1: {
    width: 284,
    height: 84,
    marginTop: 12,
    alignSelf: "center"
  },
  btexBack1: {
    width: 284,
    height: 84,
    backgroundColor: "#e63946",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  btexText1: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    height: 45,
    fontSize: 30,
    textAlign: "center",
    width: 284,
    marginTop: 20
  },
  button1: {
    top: 0,
    left: 15,
    width: 50,
    height: 50,
    position: "absolute"
  },
  image1: {
    width: 50,
    height: 50
  },
  profile1: {
    top: 20,
    left: 0,
    height: 170,
    position: "absolute",
    right: 0
  },
  profilePic1: {
    height: 170,
    width: 170,
    alignSelf: "center"
  },
  profilePic1_imageStyle: {},
  button2: {
    width: 40,
    height: 40,
    marginTop: 130,
    marginLeft: 118
  },
  btsImg1: {
    height: 40,
    width: 40
  },
  button1Stack: {
    height: 190,
    marginTop: -772
  }
});

export default Def;

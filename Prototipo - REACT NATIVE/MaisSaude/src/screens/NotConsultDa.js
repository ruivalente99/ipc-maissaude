import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function NotConsultDa(props) {
  return (
    <View style={styles.container}>
      <View style={styles.rect}>
        <View style={styles.group}>
          <Text style={styles.consulta}>Consulta</Text>
          <Text style={styles.dia}>Amanhã(6/10)</Text>
          <Text style={styles.hora}>14:25</Text>
          <Svg viewBox="0 0 170.14 169.61" style={styles.fotoDoutor}>
            <Ellipse
              strokeWidth={0}
              fill="rgba(122,121,121,1)"
              cx={85}
              cy={85}
              rx={85}
              ry={85}
            ></Ellipse>
          </Svg>
          <View style={styles.buttonConfirm}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.button}
            >
              <Text style={styles.confirmar}>Confirmar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  rect: {
    height: 560,
    backgroundColor: "#E6E6E6",
    borderRadius: 55,
    marginTop: 317
  },
  group: {
    height: 170,
    marginTop: 185
  },
  consulta: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 45,
    marginTop: -159,
    alignSelf: "center"
  },
  dia: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 45,
    marginTop: 10,
    alignSelf: "center"
  },
  hora: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    alignSelf: "center"
  },
  fotoDoutor: {
    width: 170,
    height: 170,
    marginTop: 12,
    alignSelf: "center"
  },
  buttonConfirm: {
    width: 138,
    height: 41,
    marginTop: 38,
    alignSelf: "center"
  },
  button: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderRadius: 55,
    alignSelf: "center"
  },
  confirmar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "justify",
    marginTop: 20,
    marginLeft: 70
  }
});

export default NotConsultDa;

import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, Image } from "react-native";

function ButtonBack(props) {
  return (
    <TouchableOpacity 
    style={[styles.container, props.style]}>
      <Image
        source={require("../assets/images/2961062-200.png")}
        resizeMode="contain"
        style={styles.image}
      ></Image>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {},
  image: {
    width: 45,
    height: 45
  }
});

export default ButtonBack;

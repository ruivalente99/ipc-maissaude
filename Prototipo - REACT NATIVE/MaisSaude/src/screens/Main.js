import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text,
  Linking
} from "react-native";

function Main(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <View style={styles.profile}>
        <ImageBackground
          source={require("../assets/images/photo.png")}
          resizeMode="stretch"
          style={styles.profilePic}
          imageStyle={styles.profilePic_imageStyle}
        >
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Def")}
            style={styles.button3}
          >
            <Image
              source={require("../assets/images/image_YOnL..png")}
              resizeMode="contain"
              style={styles.btsImg}
            ></Image>
          </TouchableOpacity>
        </ImageBackground>
      </View>
      <Text style={styles.welcomeMsg}>Olá, Sr Valente</Text>
      <View style={styles.buttonsMain}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("ViewMeds")}
          style={styles.buttonMeds}
        >
          <View style={styles.btmBack}>
            <Text style={styles.btmText}>💊 Medicamentos</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("ViewConsult")}
          style={styles.buttonConsult}
        >
          <View style={styles.btcBack}>
            <Text style={styles.btcText}>👨‍⚕️ Consultas</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("ViewExs")}
          style={styles.buttonEx}
        >
          <View style={styles.btexBack}>
            <Text style={styles.btexText}>🏃‍♂️ Exercício</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.bottom}>
        <View style={styles.bottomBtns}>
          <View style={styles.buttonCallRow}>
            <TouchableOpacity
              onPress={() => Linking.openURL(`tel:${112}`)}
              style={styles.buttonCall}
            >
              <Image
                source={require("../assets/images/image_gdqN..png")}
                resizeMode="contain"
                style={styles.btcallImg}
              ></Image>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Qr")}
              style={styles.button2}
            >
              <Image
                source={require("../assets/images/image_ujyH..png")}
                resizeMode="contain"
                style={styles.btqImg}
              ></Image>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  profile: {
    height: 170,
    marginTop: 60
  },
  profilePic: {
    width: 170,
    height: 170,
    alignSelf: "center"
  },
  profilePic_imageStyle: {},
  button3: {
    width: 40,
    height: 40,
    marginTop: 130,
    marginLeft: 118
  },
  btsImg: {
    height: 40,
    width: 40
  },
  welcomeMsg: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 10
  },
  buttonsMain: {
    height: 273,
    marginTop: 43
  },
  buttonMeds: {
    width: 284,
    height: 84,
    justifyContent: "center",
    alignSelf: "center"
  },
  btmBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55,
    justifyContent: "center",
    alignSelf: "center"
  },
  btmText: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 30,
    textAlign: "center"
  },
  buttonConsult: {
    width: 284,
    height: 84,
    marginTop: 11,
    alignSelf: "center"
  },
  btcBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  btcText: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 30,
    textAlign: "center",
    marginTop: 20
  },
  buttonEx: {
    width: 284,
    height: 84,
    marginTop: 11,
    alignSelf: "center"
  },
  btexBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  btexText: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 30,
    textAlign: "center",
    marginTop: 20
  },
  bottom: {
    height: 126,
    justifyContent: "center",
    marginTop: 27
  },
  bottomBtns: {
    width: 272,
    height: 126,
    flexDirection: "row",
    alignSelf: "center"
  },
  buttonCall: {
    width: 106,
    height: 106,
    marginTop: 10
  },
  btcallImg: {
    height: 106,
    width: 106
  },
  button2: {
    width: 126,
    height: 126,
    marginLeft: 40
  },
  btqImg: {
    height: 126,
    width: 126
  },
  buttonCallRow: {
    height: 126,
    flexDirection: "row",
    flex: 1
  }
});

export default Main;

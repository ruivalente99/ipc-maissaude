import React, { Component } from "react";
import { StyleSheet, View, TextInput } from "react-native";

function MaterialDisabledTextboxDuracao(props) {
  return (
    <View style={[styles.container, props.style]}>
      <TextInput
        placeholder="Duração"
        editable={false}
        placeholderTextColor="rgba(230, 230, 230,1)"
        style={styles.inputStyle}
      ></TextInput>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: "rgba(0,0,0,1)",
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center"
  },
  inputStyle: {
    color: "#000",
    paddingRight: 5,
    fontSize: 16,
    alignSelf: "stretch",
    flex: 1,
    lineHeight: 16,
    paddingTop: 16,
    paddingBottom: 8
  }
});

export default MaterialDisabledTextboxDuracao;

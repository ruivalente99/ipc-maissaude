import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text
} from "react-native";

function DoctorMain(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <View style={styles.center}>
        <View style={styles.profile}>
          <ImageBackground
            source={require("../assets/images/image_ZE78..png")}
            resizeMode="contain"
            style={styles.profilePic}
            imageStyle={styles.profilePic_imageStyle}
          >
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Def")}
              style={styles.button2}
            >
              <Image
                source={require("../assets/images/image_YOnL..png")}
                resizeMode="contain"
                style={styles.btsImg}
              ></Image>
            </TouchableOpacity>
          </ImageBackground>
        </View>
        <Text style={styles.olaDrEdgar}>Olá, Dr Edgar</Text>
        <View style={styles.group}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("ViewConsult")}
            style={styles.buttonConsult}
          >
            <View style={styles.btcBack}>
              <Text style={styles.btcText}>👨‍⚕️ Consultas</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("ReadQr")}
            style={styles.buttonEx}
          >
            <View style={styles.btexBack}>
              <Text style={styles.lerQr}>📷 Ler QR</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  center: {
    height: 460,
    marginTop: 60
  },
  profile: {
    height: 170
  },
  profilePic: {
    height: 170,
    width: 170,
   alignSelf: "center"
  },
  profilePic_imageStyle: {},
  button2: {
    width: 40,
    height: 40,
    marginTop: 130,
    marginLeft: 118
  },
  btsImg: {
    height: 40,
    width: 40
  },
  olaDrEdgar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 10
  },
  group: {
    height: 184,
    marginTop: 37
  },
  buttonConsult: {
    width: 284,
    height: 84,
    alignSelf: "center"
  },
  btcBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  btcText: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 30,
    textAlign: "center",
    marginTop: 20
  },
  buttonEx: {
    width: 284,
    height: 84,
    marginTop: 16,
    alignSelf: "center"
  },
  btexBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  lerQr: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 30,
    textAlign: "center",
    marginTop: 20
  }
});

export default DoctorMain;

import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import MaterialFixedLabelTextbox4 from "../components/MaterialFixedLabelTextbox4";
import MaterialFixedLabelTextbox6 from "../components/MaterialFixedLabelTextbox6";
import MaterialFixedLabelTextbox5 from "../components/MaterialFixedLabelTextbox5";
import MaterialFixedLabelTextbox7 from "../components/MaterialFixedLabelTextbox7";

function DefContaInformacao(props) {
  return (
    <View style={styles.container}>
      <View style={styles.buttonStack}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button}
        >
          <Image
            source={require("../assets/images/2961062-200.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
        </TouchableOpacity>
        <View style={styles.adminImagemLinguagem}>
          <View style={styles.imagemPrincipal1}>
            <Image
              source={require("../assets/images/image_XKVx..png")}
              resizeMode="contain"
              style={styles.image2}
            ></Image>
            <Text style={styles.conta3}>Conta</Text>
          </View>
        </View>
      </View>
      <View style={styles.painelDefinicoes1}>
        <View style={styles.retangulo}>
          <View style={styles.definicoes1}>
            <View style={styles.nomeTextBox1}>
              <View style={styles.inputNomeStack}>
                <MaterialFixedLabelTextbox4
                  style={styles.inputNome}
                ></MaterialFixedLabelTextbox4>
                <View style={styles.nineAmScroll1}></View>
                <View style={styles.rect1}></View>
              </View>
            </View>
            <View style={styles.campoContactoProprio}>
              <View style={styles.inputContactoStack}>
                <MaterialFixedLabelTextbox6
                  style={styles.inputContacto}
                ></MaterialFixedLabelTextbox6>
                <View style={styles.rect2}></View>
                <View style={styles.rect3}></View>
              </View>
            </View>
            <View style={styles.campoEmergencia}>
              <View style={styles.rect4Row}>
                <View style={styles.rect4}></View>
                <View style={styles.inputCampoEmergenciaStack}>
                  <MaterialFixedLabelTextbox5
                    style={styles.inputCampoEmergencia}
                  ></MaterialFixedLabelTextbox5>
                  <View style={styles.rect5}></View>
                </View>
              </View>
            </View>
            <View style={styles.campoMorada}>
              <View style={styles.inputMoradaStack}>
                <MaterialFixedLabelTextbox7
                  style={styles.inputMorada}
                ></MaterialFixedLabelTextbox7>
                <View style={styles.rect7}></View>
                <View style={styles.rect8}></View>
              </View>
            </View>
            <View style={styles.bTmId1}>
              <View style={styles.rect6}>
                <View style={styles.image3Row}>
                  <Image
                    source={require("../assets/images/image_HLrI..png")}
                    resizeMode="contain"
                    style={styles.image3}
                  ></Image>
                  <Text style={styles.contacto2}>+ Contacto</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  button: {
    top: 0,
    left: 9,
    width: 46,
    height: 45,
    position: "absolute"
  },
  image: {
    width: 46,
    height: 45
  },
  adminImagemLinguagem: {
    top: 19,
    left: 0,
    height: 257,
    position: "absolute",
    right: 0
  },
  imagemPrincipal1: {
    width: 200,
    height: 257,
    alignSelf: "center"
  },
  image2: {
    height: 200,
    width: 200
  },
  conta3: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    flex: 1,
    marginBottom: -37,
    marginTop: 20,
    alignSelf: "center"
  },
  buttonStack: {
    height: 276,
    marginTop: 40
  },
  painelDefinicoes1: {
    height: 481,
    marginTop: 15
  },
  retangulo: {
    height: 481,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 39,
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  definicoes1: {
    width: 375,
    height: 434,
    alignSelf: "center"
  },
  nomeTextBox1: {
    width: 300,
    height: 54,
    marginTop: 28,
    marginLeft: 29
  },
  inputNome: {
    height: 43,
    width: 300,
    position: "absolute",
    top: 104,
    left: 0
  },
  nineAmScroll1: {
    top: 93,
    left: 0,
    width: 1,
    height: 53,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)"
  },
  rect1: {
    top: 0,
    left: 153,
    width: 1,
    height: 293,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)",
    transform: [
      {
        rotate: "90.00deg"
      }
    ]
  },
  inputNomeStack: {
    width: 300,
    height: 293,
    marginTop: -93
  },
  campoContactoProprio: {
    width: 300,
    height: 54,
    marginTop: 15,
    marginLeft: 29
  },
  inputContacto: {
    height: 43,
    width: 300,
    position: "absolute",
    top: 104,
    left: 0
  },
  rect2: {
    top: 93,
    left: 0,
    width: 1,
    height: 53,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)"
  },
  rect3: {
    top: 0,
    left: 153,
    width: 1,
    height: 293,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)",
    transform: [
      {
        rotate: "90.00deg"
      }
    ]
  },
  inputContactoStack: {
    width: 300,
    height: 293,
    marginTop: -93
  },
  campoEmergencia: {
    width: 300,
    height: 54,
    flexDirection: "row",
    marginTop: 21,
    marginLeft: 29
  },
  rect4: {
    width: 1,
    height: 53,
    backgroundColor: "rgba(0,0,0,1)",
    marginTop: 93
  },
  inputCampoEmergencia: {
    height: 43,
    width: 300,
    position: "absolute",
    top: 104,
    left: 0
  },
  rect5: {
    top: 0,
    left: 152,
    width: 1,
    height: 293,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)",
    transform: [
      {
        rotate: "90.00deg"
      }
    ]
  },
  inputCampoEmergenciaStack: {
    width: 300,
    height: 293
  },
  rect4Row: {
    height: 293,
    flexDirection: "row",
    flex: 1,
    marginRight: -1,
    marginTop: -93
  },
  campoMorada: {
    width: 300,
    height: 54,
    marginTop: 27,
    marginLeft: 29
  },
  inputMorada: {
    height: 43,
    width: 300,
    position: "absolute",
    left: 0,
    top: 103
  },
  rect7: {
    top: 93,
    left: 0,
    width: 1,
    height: 53,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)"
  },
  rect8: {
    top: 0,
    left: 153,
    width: 1,
    height: 293,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)",
    transform: [
      {
        rotate: "90.00deg"
      }
    ]
  },
  inputMoradaStack: {
    width: 300,
    height: 293,
    marginTop: -93
  },
  bTmId1: {
    width: 284,
    height: 84,
    marginTop: 27,
    marginLeft: 46
  },
  rect6: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(191,191,191,1)",
    borderRadius: 55,
    flexDirection: "row",
    alignSelf: "center"
  },
  image3: {
    height: 65,
    width: 65
  },
  contacto2: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 24,
    width: 136,
    height: 29,
    marginLeft: 17,
    marginTop: 18
  },
  image3Row: {
    height: 65,
    flexDirection: "row",
    flex: 1,
    marginRight: 39,
    marginLeft: 27,
    marginTop: 10
  }
});

export default DefContaInformacao;

import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';

const getCurrentDate=()=>{

  var date = new Date().getDate();
  var month = new Date().getMonth() + 1;
  var year = new Date().getFullYear();

  //Alert.alert(date + '-' + month + '-' + year);
  // You can turn it in to your desired format
  return date + '-' + month + '-' + year;//format: dd-mm-yyyy;
}
function ViewConsult(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.buttonback}
      >
        <Image
          source={require("../assets/images/2961062-2001.png")}
          resizeMode="contain"
          style={styles.btnback}
        ></Image>
      </TouchableOpacity>
      <View style={styles.group2}>
      
        <Calendar 
           // Collection of dates that have to be marked. Default = {}
          markedDates={{
            getCurrentDate : {selected: true, marked: true, selectedColor: 'blue'}
          }}
        />
        
        <View style={styles.btConsults}>
          <View style={styles.btmId1}>
            <View style={styles.btmBack3}>
              <View style={styles.btmImg3Row}>
                <Svg viewBox="0 0 60 60" style={styles.btmImg3}>
                  <Ellipse
                    stroke="rgba(230, 230, 230,1)"
                    strokeWidth={0}
                    fill="rgba(230, 230, 230,1)"
                    cx={30}
                    cy={30}
                    rx={30}
                    ry={30}
                  ></Ellipse>
                </Svg>
                <Text style={styles.drEdgar}>Dr. Edgar</Text>
                <Text style={styles.btmTextDose3}>10:20</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  buttonback: {
    width: 50,
    height: 50,
    marginTop: 40,
    marginLeft: 15
  },
  btnback: {
    width: 50,
    height: 50
  },
  group2: {
    height: 444,
    width:'80%',
    marginTop: 26,
    alignSelf: "center",
  },
  calendario: {
    height: 300,
  },
  back: {
    width: 300,
    backgroundColor: "#E6E6E6",
    height: 300,
    alignSelf: "center"
  },
  btConsults: {
    height: 84,
    marginTop: 59
  },
  btmId1: {
    width: 284,
    height: 84,
    overflow: "visible",
    alignSelf: "center"
  },
  btmBack3: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg3: {
    width: 60,
    height: 60
  },
  drEdgar: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 99,
    height: 29,
    marginLeft: 16,
    marginTop: 16
  },
  btmTextDose3: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 24,
    marginLeft: 12,
    marginTop: 16
  },
  btmImg3Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 22,
    marginLeft: 16,
    marginTop: 12
  }
});

export default ViewConsult;

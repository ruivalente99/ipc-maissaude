import React, { Component } from "react";
import { StyleSheet, View, TextInput } from "react-native";

function MaterialDisabledTextboxNome(props) {
  return (
    <View style={[styles.container, props.style]}>
      <TextInput
        placeholder="Nome"
        editable={false}
        placeholderTextColor="rgba(230, 230, 230,1)"
        style={styles.inputStyle}
      ></TextInput>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#000000",
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 0
  },
  inputStyle: {
    color: "#000",
    paddingRight: 5,
    fontSize: 16,
    alignSelf: "stretch",
    flex: 1,
    lineHeight: 16,
    paddingTop: 16,
    paddingBottom: 8
  }
});

export default MaterialDisabledTextboxNome;

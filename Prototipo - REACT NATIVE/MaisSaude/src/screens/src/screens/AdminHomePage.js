import React, { Component } from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function AdminHomePage(props) {
  return (
    <View style={styles.container}>
      <View style={styles.adminImage}>
        <View style={styles.imagemPrincipal}>
          <View style={styles.imageStack}>
            <Image
              source={require("../assets/images/image_XKVx..png")}
              resizeMode="contain"
              style={styles.image}
            ></Image>
            <View style={styles.buttonSettings}>
              <Image
                source={require("../assets/images/image_qki4..png")}
                resizeMode="contain"
                style={styles.bTSettingsImg}
              ></Image>
            </View>
          </View>
          <Text style={styles.adminFerreira}>Admin Ferreira</Text>
        </View>
      </View>
      <View style={styles.buttons}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("AprovarMed")}
          style={styles.button2}
        >
          <View style={styles.bTAprovaMed}>
            <View style={styles.group2Row}>
              <View style={styles.group2}>
                <View style={styles.group}>
                  <Svg viewBox="0 0 60.43 59.78" style={styles.bTImg}>
                    <Ellipse
                      stroke="rgba(230, 230, 230,1)"
                      strokeWidth={0}
                      fill="rgba(155,155,155,1)"
                      cx={30}
                      cy={30}
                      rx={30}
                      ry={30}
                    ></Ellipse>
                  </Svg>
                </View>
              </View>
              <Text style={styles.aprovarMedicos}>Aprovar Médicos</Text>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("BloquearMed")}
          style={styles.button3}
        >
          <View style={styles.bTBloquearUser}>
            <View style={styles.bTImg2Row}>
              <Svg viewBox="0 0 60.43 59.78" style={styles.bTImg2}>
                <Ellipse
                  stroke="rgba(230, 230, 230,1)"
                  strokeWidth={0}
                  fill="rgba(155,155,155,1)"
                  cx={30}
                  cy={30}
                  rx={30}
                  ry={30}
                ></Ellipse>
              </Svg>
              <Text style={styles.bloquearUtilizador3}>
                Bloquear Utilizador
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <View style={styles.buttonAdicionaAdmin}>
          <View style={styles.bTAddAdmin}>
            <Text style={styles.bloquearUtilizador2}>Adicionar ADMIN</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  adminImage: {
    height: 272,
    marginTop: 59
  },
  imagemPrincipal: {
    width: 293,
    height: 272,
    alignSelf: "center"
  },
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    height: 200,
    width: 200
  },
  buttonSettings: {
    top: 152,
    left: 144,
    width: 60,
    height: 60,
    position: "absolute"
  },
  bTSettingsImg: {
    height: 60,
    width: 60
  },
  imageStack: {
    width: 204,
    height: 212,
    marginLeft: 47
  },
  adminFerreira: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 45,
    marginTop: 6
  },
  buttons: {
    height: 311,
    marginTop: 29
  },
  button2: {
    width: 284,
    height: 84,
    alignSelf: "center"
  },
  bTAprovaMed: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderRadius: 55,
    borderWidth: 0,
    borderColor: "#000000",
    flexDirection: "row"
  },
  group2: {
    width: 60,
    height: 60
  },
  group: {
    width: 60,
    height: 60
  },
  bTImg: {
    width: 60,
    height: 60
  },
  aprovarMedicos: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    marginLeft: 9,
    marginTop: 19
  },
  group2Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 26,
    marginLeft: 8,
    marginTop: 12
  },
  button3: {
    width: 284,
    height: 84,
    marginTop: 26,
    alignSelf: "center"
  },
  bTBloquearUser: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(230,57,70,1)",
    borderRadius: 55,
    borderWidth: 0,
    borderColor: "#000000",
    flexDirection: "row"
  },
  bTImg2: {
    width: 60,
    height: 60
  },
  bloquearUtilizador3: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    marginLeft: 7,
    marginTop: 19
  },
  bTImg2Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 6,
    marginLeft: 8,
    marginTop: 12
  },
  buttonAdicionaAdmin: {
    width: 284,
    height: 84,
    marginTop: 33,
    alignSelf: "center"
  },
  bTAddAdmin: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(191,191,191,1)",
    borderRadius: 55,
    borderWidth: 0,
    borderColor: "#000000"
  },
  bloquearUtilizador2: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    marginTop: 31,
    marginLeft: 50
  }
});

export default AdminHomePage;

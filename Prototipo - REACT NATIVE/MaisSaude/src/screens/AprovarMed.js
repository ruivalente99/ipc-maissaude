import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, Image } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import CupertinoSearchBarBasic from "../components/CupertinoSearchBarBasic";

function AprovarMed(props) {
  return (
    <View style={styles.container}>
      <View style={styles.selecaoMedicos}>
        <View style={styles.medicos}>
          <View style={styles.nineAmScroll1}></View>
          <View style={styles.nineAmScroll1Filler}></View>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("AprovarDropdown")}
            style={styles.button2}
          >
            <View style={styles.btmBack1}>
              <View style={styles.btmImg1Row}>
                <Svg viewBox="0 0 60 60" style={styles.btmImg1}>
                  <Ellipse
                    stroke="rgba(230, 230, 230,1)"
                    strokeWidth={0}
                    fill="rgba(230, 230, 230,1)"
                    cx={30}
                    cy={30}
                    rx={30}
                    ry={30}
                  ></Ellipse>
                </Svg>
                <View style={styles.drGuinchoStack}>
                  <Text style={styles.drGuincho}>Dr. Guincho</Text>
                  <Image
                    source={require("../assets/images/image_cOMu..png")}
                    resizeMode="contain"
                    style={styles.corretoImg}
                  ></Image>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.searchBar}>
        <CupertinoSearchBarBasic
          style={styles.searchBarMed}
        ></CupertinoSearchBarBasic>
      </View>
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.button}
      >
        <Image
          source={require("../assets/images/2961062-200.png")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  selecaoMedicos: {
    height: 143,
    marginTop: 190
  },
  medicos: {
    width: 365,
    height: 143,
    flexDirection: "row",
    alignSelf: "center"
  },
  nineAmScroll1: {
    width: 1,
    height: 108,
    backgroundColor: "rgba(230, 230, 230,1)",
    marginTop: 25
  },
  nineAmScroll1Filler: {
    flex: 1,
    flexDirection: "row"
  },
  button2: {
    width: 284,
    height: 84,
    overflow: "visible",
    marginRight: 42,
    marginTop: 42
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg1: {
    width: 60,
    height: 60
  },
  drGuincho: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    left: 0,
    top: 0,
    width: 143,
    height: 29
  },
  corretoImg: {
    position: "absolute",
    top: 2,
    left: 138,
    height: 25,
    width: 25
  },
  drGuinchoStack: {
    width: 163,
    height: 29,
    marginLeft: 29,
    marginTop: 16
  },
  btmImg1Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 16,
    marginLeft: 16,
    marginTop: 12
  },
  searchBar: {
    height: 34,
    marginTop: -203
  },
  searchBarMed: {
    height: 34,
    width: 343,
    backgroundColor: "#1a5b92",
    alignSelf: "center"
  },
  button: {
    width: 50,
    height: 50,
    marginTop: -120,
    marginLeft: 15
  },
  image: {
    width: 46,
    height: 45
  }
});

export default AprovarMed;

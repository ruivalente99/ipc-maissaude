import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image
} from "react-native";
import MaterialRadio from "../components/MaterialRadio";

function DefLinguagem(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Text style={styles.linguagem}>Linguagem</Text>
      <View style={styles.menu}>
        <View style={styles.background}>
          <View style={styles.group3}>
            <View style={styles.group}>
              <View style={styles.materialRadioRow}>
                <MaterialRadio style={styles.materialRadio}></MaterialRadio>
                <Text style={styles.portugues}>Português</Text>
              </View>
            </View>
            <View style={styles.group1}>
              <View style={styles.materialRadio1Row}>
                <MaterialRadio style={styles.materialRadio}></MaterialRadio>
                <Text style={styles.portugues1}>Inglês</Text>
              </View>
            </View>
            <View style={styles.group2}>
              <View style={styles.materialRadio2Row}>
                <MaterialRadio style={styles.materialRadio}></MaterialRadio>
                <Text style={styles.portugues2}>Francês</Text>
              </View>
            </View>
            <View style={styles.group2}>
              <View style={styles.materialRadio2Row}>
                <MaterialRadio style={styles.materialRadio}></MaterialRadio>
                <Text style={styles.portugues2}>Mandarim</Text>
              </View>
            </View>
            <View style={styles.group2}>
              <View style={styles.materialRadio2Row}>
                <MaterialRadio style={styles.materialRadio}></MaterialRadio>
                <Text style={styles.portugues2}>Japonês</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.button1Stack}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button1}
        >
          <Image
            source={require("../assets/images/2961062-2001.png")}
            resizeMode="contain"
            style={styles.image1}
          ></Image>
        </TouchableOpacity>
        <View style={styles.profile1}>
          <Image
            source={require("../assets/images/image_XKVx..png")}
            resizeMode="contain"
            style={styles.profilePic1}
          ></Image>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  linguagem: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 240
  },
  menu: {
    height: 492,
    marginTop: 39
  },
  background: {
    height: 492,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  group3: {
    width: 163,
    height: 142,
    marginTop: 100,
    alignSelf: "center",
    right:20
  },
  group: {
    width: 163,
    height: 40,
    flexDirection: "row"
  },
  materialRadio: {
    height: 40,
    width: 40
  },
  portugues: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    marginLeft: 13,
    marginTop: 6
  },
  materialRadioRow: {
    height: 40,
    flexDirection: "row",
    flex: 1
  },
  group1: {
    width: 163,
    height: 40,
    flexDirection: "row",
    marginTop: 11
  },
  materialRadio1: {
    height: 40,
    width: 40
  },
  portugues1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    marginLeft: 13,
    marginTop: 6
  },
  materialRadio1Row: {
    height: 40,
    flexDirection: "row",
    flex: 1
  },
  group2: {
    width: 163,
    height: 40,
    flexDirection: "row",
    marginTop: 11
  },
  portugues2: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    marginLeft: 13,
    marginTop: 6
  },
  materialRadio2Row: {
    height: 40,
    flexDirection: "row",
    flex: 1
  },
  button1: {
    top: 0,
    left: 15,
    width: 50,
    height: 50,
    position: "absolute"
  },
  image1: {
    width: 50,
    height: 50
  },
  profile1: {
    top: 20,
    left: 0,
    height: 170,
    position: "absolute",
    right: 0
  },
  profilePic1: {
    height: 170,
    width: 170,
    alignSelf: "center"
  },
  button1Stack: {
    height: 190,
    marginTop: -772
  }
});

export default DefLinguagem;

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text
} from "react-native";
import WhiteBoard from "../components/WhiteBoard";

function Qr(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <View style={styles.profileStack}>
        <View style={styles.profile}>
          <ImageBackground
            source={require("../assets/images/photo.png")}
            resizeMode="stretch"
            style={styles.profilePic}
            imageStyle={styles.profilePic_imageStyle}
          >
              <View style={styles.buttonSettings}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Def")}
            style={styles.button3}
          >
            <Image
              source={require("../assets/images/image_YOnL..png")}
              resizeMode="contain"
              style={styles.btsImg}
            ></Image>
          </TouchableOpacity>
          </View>
          </ImageBackground>
        </View>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button1}
        >
          <Image
            source={require("../assets/images/2961062-200.png")}
            resizeMode="contain"
            style={styles.image1}
          ></Image>
        </TouchableOpacity>
      </View>
      <Text style={styles.sessao}>Sessão</Text>
      <View style={styles.menu}>
        
        <View style={styles.background}>
          <ImageBackground
            source={require("../assets/images/image_Yo4w..png")}
            resizeMode="contain"
            style={styles.qRImg}
            imageStyle={styles.qRImg_imageStyle}
          >
            <Text style={styles.id42069}>ID: 42069</Text>
          </ImageBackground>
        
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  profile: {
    top: 20,
    left: 0,
    height: 170,
    position: "absolute",
    right: 0
  },
  profilePic: {
    width: 170,
    height: 170,
    alignSelf: "center"
  },
  profilePic_imageStyle: {},
  buttonSettings: {
    width: 40,
    height: 40,
    marginTop: 130,
    marginLeft: 118
  },
  btsImg: {
    height: 40,
    width: 40
  },
  button1: {
    top: 0,
    left: 15,
    width: 50,
    height: 50,
    position: "absolute"
  },
  image1: {
    width: 50,
    height: 50
  },
  profileStack: {
    height: 190,
    marginTop: 40
  },
  sessao: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 10
  },
  menu: {
    height: 492,
    marginTop: 39
  },
  background: {
    height: 492,
    backgroundColor: "#E6E6E6",
    borderWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  qRImg: {
    height: 385,
    marginTop: 35
  },
  qRImg_imageStyle: {},
  id42069: {
    fontFamily: "roboto-regular",
    color: "rgba(26,91,146,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 325
  }
});

export default Qr;

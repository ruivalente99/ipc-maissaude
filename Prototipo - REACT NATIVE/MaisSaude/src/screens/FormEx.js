import React, { Component } from "react";
import MyDatePicker from "../components/myDatePicker";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput
} from "react-native";

function FormEx(props) {
  return (
    <View style={styles.container}>
      <View style={styles.topBtnStackColumn}>
        <View style={styles.topBtnStack}>
          <View style={styles.topBtn}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.button1}
            >
              <Image
                source={require("../assets/images/2961062-2001.png")}
                resizeMode="contain"
                style={styles.image1}
              ></Image>
            </TouchableOpacity>
            <View style={styles.button1Filler}></View>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Main")}
              style={styles.button2}
            >
              <Image
                source={require("../assets/images/unknown.png")}
                resizeMode="contain"
                style={styles.image2}
              ></Image>
            </TouchableOpacity>
          </View>
          <Text style={styles.exercicio}>Exercício</Text>
        </View>
        <View style={styles.calendario1} >
            <MyDatePicker/>
        </View>
        <View style={styles.group}>
          <View style={styles.nome1}>
            <TextInput
              placeholder="Nome"
              keyboardType="default"
              maxLength={8}
              returnKeyType="go"
              disableFullscreenUI={true}
              placeholderTextColor="rgba(230, 230, 230,1)"
              style={styles.textInput1}
            ></TextInput>
            <View style={styles.rect1}></View>
          </View>
          <View style={styles.nome2}>
            <TextInput
              placeholder="Duração / Repetições"
              keyboardType="default"
              maxLength={8}
              returnKeyType="go"
              disableFullscreenUI={true}
              placeholderTextColor="rgba(230, 230, 230,1)"
              style={styles.textInput2}
            ></TextInput>
            <View style={styles.rect2}></View>
          </View>
        </View>
      </View>
      <View style={styles.topBtnStackColumnFiller}></View>
      <TouchableOpacity
        onPress={() => props.navigation.navigate("Main")}
        style={styles.buttonMeds1}
      >
        <View style={styles.btmBack1}>
          <Text style={styles.adicionar}>Adicionar</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1a5b92"
  },
  topBtn: {
    top: 0,
    left: 15,
    width: 345,
    height: 50,
    position: "absolute",
    flexDirection: "row"
  },
  button1: {
    width: 50,
    height: 50
  },
  calendario1: {
    width:'80%',
    top: 15,
    alignSelf: "center",
  },
  image1: {
    width: 50,
    height: 50
  },
  button1Filler: {
    flex: 1,
    flexDirection: "row"
  },
  button2: {
    width: 50,
    height: 50
  },
  image2: {
    width: 50,
    height: 50
  },
  exercicio: {
    top: 44,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    left: 0,
    right: 0,
    textAlign: "center"
  },
  topBtnStack: {
    height: 85
  },
  group: {
    width: 375,
    height: 91,
    marginTop: 50,
    alignSelf: "center"
  },
  nome1: {
    height: 40,
    width: 375
  },
  textInput1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect1: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  nome2: {
    height: 40,
    marginTop: 11
  },
  textInput2: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect2: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  topBtnStackColumn: {
    marginTop: 40
  },
  topBtnStackColumnFiller: {
    flex: 1
  },
  buttonMeds1: {
    width: 284,
    height: 84,
    marginBottom: 84,
    alignSelf: "center"
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55,
   
  },
  adicionar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    height: 33,
    fontSize: 24,
    textAlign: "center",
    width: 284,
   
    marginTop: 26
  }
});

export default FormEx;

import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity} from "react-native";
import DateTimePicker from '@react-native-community/datetimepicker';

function MyDatePicker(props) {
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };
  return (
    <View>
    <Text style={styles.agendar}>Dia</Text>
    <View style={styles.btmBack2}>
        <TouchableOpacity
        onPress={showDatepicker}
       
      >
      <Text style={styles.agendar}>{date.getDate()} / {date.getMonth()+1} </Text>
      </TouchableOpacity>
    </View>
    <Text style={styles.agendar}>Hora</Text>
    <View style={styles.btmBack2}>
        <TouchableOpacity
        onPress={showTimepicker}
        
      >
        
      <Text style={styles.agendar}>{date.getHours()} : {date.getMinutes()} </Text>
      </TouchableOpacity>
    </View>
    
    {show && (
      <DateTimePicker
        testID="dateTimePicker"
        value={date}
        mode={mode}
        is24Hour={true}
        display="default"
        onChange={onChange}
      />
    )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  btmBack2: {
    marginTop:10,
    width: 165,
    height: 84,
    backgroundColor: "rgba(0,0,0,0.2)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55,
    alignSelf: "center"
  },
  agendar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 26
  }
});

export default MyDatePicker;

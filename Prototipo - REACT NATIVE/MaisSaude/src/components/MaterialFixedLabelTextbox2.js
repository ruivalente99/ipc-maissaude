import React, { Component } from "react";
import { StyleSheet, View, Text, TextInput } from "react-native";

function MaterialFixedLabelTextbox2(props) {
  return (
    <View style={[styles.container, props.style]}>
      <Text style={styles.mail}>Mail</Text>
      <TextInput style={styles.inputStyle}></TextInput>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: "rgba(0,0,0,1)",
    flexDirection: "row",
    paddingLeft: 16
  },
  mail: {
    fontSize: 20,
    lineHeight: 16,
    paddingTop: 16,
    paddingBottom: 8,
    color: "rgba(129,129,129,1)",
    alignSelf: "flex-start"
  },
  inputStyle: {
    color: "#000",
    paddingRight: 5,
    fontSize: 16,
    alignSelf: "stretch",
    flex: 1,
    lineHeight: 16,
    paddingTop: 14,
    paddingBottom: 8,
    paddingLeft: 30
  }
});

export default MaterialFixedLabelTextbox2;

import React, { Component } from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import CupertinoSearchBarBasic from "../components/CupertinoSearchBarBasic";

function BloquearMed(props) {
  return (
    <View style={styles.container}>
      <View style={styles.escolhaMedico}>
        <View style={styles.medicos}>
          <View style={styles.nineAmScroll1Row}>
            <View style={styles.nineAmScroll1}></View>
            <View style={styles.nineAmScroll1Filler}></View>
            <View style={styles.btmId1Column}>
              <View style={styles.btmId1}>
                <View style={styles.btmBack1}>
                  <View style={styles.btmImg1Row}>
                    <Svg viewBox="0 0 60 60" style={styles.btmImg1}>
                      <Ellipse
                        stroke="rgba(230, 230, 230,1)"
                        strokeWidth={0}
                        fill="rgba(230, 230, 230,1)"
                        cx={30}
                        cy={30}
                        rx={30}
                        ry={30}
                      ></Ellipse>
                    </Svg>
                    <View style={styles.srLouroStack}>
                      <Text style={styles.srLouro}>Sr. Louro</Text>
                      <Image
                        source={require("../assets/images/image_rG59..png")}
                        resizeMode="contain"
                        style={styles.bloquearImg}
                      ></Image>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.btmId2}>
                <View style={styles.rect}>
                  <View style={styles.ellipseRow}>
                    <Svg viewBox="0 0 60 60" style={styles.ellipse}>
                      <Ellipse
                        stroke="rgba(230, 230, 230,1)"
                        strokeWidth={0}
                        fill="rgba(230, 230, 230,1)"
                        cx={30}
                        cy={30}
                        rx={30}
                        ry={30}
                      ></Ellipse>
                    </Svg>
                    <View style={styles.drGanchoStack}>
                      <Text style={styles.drGancho}>Dr. Gancho</Text>
                      <Image
                        source={require("../assets/images/image_rG59..png")}
                        resizeMode="contain"
                        style={styles.image3}
                      ></Image>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.button}
      >
        <Image
          source={require("../assets/images/2961062-200.png")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
      </TouchableOpacity>
      <View style={styles.searchBar1}>
        <CupertinoSearchBarBasic
          style={styles.searchBarMed1}
        ></CupertinoSearchBarBasic>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  escolhaMedico: {
    height: 249,
    marginTop: 189
  },
  medicos: {
    width: 365,
    height: 249,
    alignSelf: "center"
  },
  nineAmScroll1: {
    width: 1,
    height: 216,
    backgroundColor: "rgba(230, 230, 230,1)"
  },
  nineAmScroll1Filler: {
    flex: 1,
    flexDirection: "row"
  },
  btmId1: {
    width: 284,
    height: 84,
    overflow: "visible"
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg1: {
    width: 60,
    height: 60
  },
  srLouro: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    left: 0,
    top: 0,
    width: 143,
    height: 29
  },
  bloquearImg: {
    position: "absolute",
    top: 2,
    left: 138,
    height: 25,
    width: 26
  },
  srLouroStack: {
    width: 164,
    height: 29,
    marginLeft: 29,
    marginTop: 16
  },
  btmImg1Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 15,
    marginLeft: 16,
    marginTop: 12
  },
  btmId2: {
    width: 284,
    height: 84,
    overflow: "visible",
    marginTop: 20
  },
  rect: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 55,
    flexDirection: "row"
  },
  ellipse: {
    width: 60,
    height: 60
  },
  drGancho: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    left: 0,
    top: 0,
    width: 143,
    height: 29
  },
  image3: {
    position: "absolute",
    top: 2,
    left: 138,
    height: 25,
    width: 26
  },
  drGanchoStack: {
    width: 164,
    height: 29,
    marginLeft: 29,
    marginTop: 16
  },
  ellipseRow: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 15,
    marginLeft: 16,
    marginTop: 12
  },
  btmId1Column: {
    width: 284,
    alignItems: "flex-end",
    marginTop: 17,
    marginBottom: 11
  },
  nineAmScroll1Row: {
    height: 216,
    flexDirection: "row",
    marginTop: 25,
    marginRight: 42
  },
  button: {
    width: 46,
    height: 45,
    marginTop: -398,
    marginLeft: 9
  },
  image: {
    width: 46,
    height: 45
  },
  searchBar1: {
    height: 34,
    marginTop: 45
  },
  searchBarMed1: {
    height: 34,
    width: 343,
    backgroundColor: "#1a5b92",
    alignSelf: "center"
  }
});

export default BloquearMed;

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput
} from "react-native";

function LoginEd(props) {
  return (
    <View style={styles.container}>
      <View style={styles.topBtnStackColumn}>
        <View style={styles.topBtnStack}>
          <View style={styles.topBtn}>
            <TouchableOpacity
              onPress={() => props.navigation.goBack()}
              style={styles.button1}
            >
              <Image
                source={require("../assets/images/2961062-2001.png")}
                resizeMode="contain"
                style={styles.image1}
              ></Image>
            </TouchableOpacity>
          </View>
          <Text style={styles.iniciarSessao}>Iniciar sessão</Text>
        </View>
        <View style={styles.group}>
          <View style={styles.nome1}>
            <TextInput
              placeholder="Nome de utilizador ou email"
              keyboardType="default"
              maxLength={8}
              returnKeyType="go"
              disableFullscreenUI={true}
              placeholderTextColor="rgba(230, 230, 230,1)"
              style={styles.textInput1}
            ></TextInput>
            <View style={styles.rect1}></View>
          </View>
          <View style={styles.nome2}>
            <TextInput
              placeholder="Palavra-passe"
              keyboardType="default"
              maxLength={8}
              returnKeyType="go"
              disableFullscreenUI={true}
              placeholderTextColor="rgba(230, 230, 230,1)"
              secureTextEntry={true}
              style={styles.textInput2}
            ></TextInput>
            <View style={styles.rect2}></View>
          </View>
        </View>
        <TouchableOpacity style={styles.button2}>
          <Text style={styles.loremIpsum}>Esqueci-me da palavra-passe</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button3}
         onPress={() => props.navigation.navigate("Login")}>
          <Text style={styles.criarConta}>Criar conta</Text>
        </TouchableOpacity>
        <View style={styles.profile1}>
          <Image
            source={require("../assets/images/professional-email-signature-icon.jpg")}
            resizeMode="cover"
            style={styles.profilePic1}
          ></Image>
        </View>
      </View>
      <View style={styles.topBtnStackColumnFiller}></View>
      <TouchableOpacity
        onPress={() => props.navigation.navigate("Main")}
        style={styles.buttonMeds1}
      >
        <View style={styles.btmBack1}>
          <Text style={styles.iniciarSessao2}>Iniciar sessão</Text>
        </View>
      </TouchableOpacity>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1a5b92"
  },
  topBtn: {
    top: 0,
    left: 15,
    width: 345,
    height: 50,
    position: "absolute"
  },
  button1: {
    width: 50,
    height: 50
  },
  image1: {
    width: 50,
    height: 50
  },
  iniciarSessao: {
    top: 44,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    left: 0,
    right: 0,
    textAlign: "center"
  },
  topBtnStack: {
    height: 85
  },
  group: {
    width: 375,
    height: 91,
    marginTop: 236,
    alignSelf: "center"
  },
  nome1: {
    height: 40,
    width: 375
  },
  textInput1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect1: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  nome2: {
    height: 40,
    marginTop: 11
  },
  textInput2: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect2: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  button2: {
    height: 29,
    marginTop: 19,
    alignSelf: "center"
  },
  loremIpsum: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    opacity: 0.5
  },
  button3: {
    width: 117,
    height: 29,
    marginTop: 31,
    alignSelf: "center"
  },
  criarConta: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    opacity: 0.5
  },
  profile1: {
    height: 170,
    justifyContent: "center",
    marginTop: -402
  },
  profilePic1: {
    width: 170,
    height: 170,
    borderRadius: 100,
    alignSelf: "center"
  },
  image2: {
    width: 50,
    height: 50,
    marginTop: "80%",
    marginLeft: 163
  },
  topBtnStackColumn: {
    marginTop: 40
  },
  topBtnStackColumnFiller: {
    flex: 1
  },
  buttonMeds1: {
    width: 284,
    height: 84,
    marginBottom: 128,
    alignSelf: 'center'
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55,
    marginTop: 26,
    alignSelf: "center"
  },
  iniciarSessao2: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    height: 33,
    fontSize: 24,
    textAlign: "center",
    width: 284,
    marginTop: 26
  }
});

export default LoginEd;

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";

function Login(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <View style={styles.profile}>
        <Image
          source={require("../assets/images/professional-email-signature-icon.jpg")}
          resizeMode="cover"
          style={styles.profilePic}
        ></Image>
      </View>
      <Text style={styles.criarConta}>Criar Conta</Text>
      <View style={styles.menu}>
        <View style={styles.background}>
          <View style={styles.inputs}>
            <View style={styles.nomeColumn}>
              <View style={styles.nome}>
                <TextInput
                  placeholder="Nome"
                  keyboardType="default"
                  maxLength={8}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  style={styles.textInput2}
                ></TextInput>
                <View style={styles.rect2}></View>
              </View>
              <View style={styles.contatoPro}>
                <TextInput
                  placeholder="E-mail"
                  keyboardType="email-address"
                  maxLength={8}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  dataDetector="all"
                  style={styles.textInput1}
                ></TextInput>
                <View style={styles.rect1}></View>
              </View>
              <View style={styles.contatoPro1}>
                <TextInput
                  placeholder="Idade"
                  keyboardType="number-pad"
                  maxLength={8}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  dataDetector="all"
                  style={styles.textInput3}
                ></TextInput>
                <View style={styles.rect3}></View>
              </View>
              <View style={styles.morada}>
                <TextInput
                  placeholder="Password"
                  keyboardType="default"
                  maxLength={8}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  dataDetector="address"
                  secureTextEntry={true}
                  style={styles.textInput4}
                ></TextInput>
                <View style={styles.rect4}></View>
              </View>
            </View>
            <View style={styles.nomeColumnFiller}></View>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Main")}
              style={styles.buttonMeds2}
            >
              <View style={styles.btmBack2}>
                <Text style={styles.registar}>Registar</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  profile: {
    height: 170,
    justifyContent: "center",
    marginTop: 60
  },
  profilePic: {
    width: 170,
    height: 170,
    borderRadius: 100,
    alignSelf: "center"
  },
  criarConta: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 10
  },
  menu: {
    height: 492,
    marginTop: 39
  },
  background: {
    height: 492,
    backgroundColor: "#E6E6E6",
    borderWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  inputs: {
    height: 300,
    marginTop: 96,
    alignSelf: "center"
  },
  nome: {
    height: 40,
    marginRight: 1
  },
  textInput2: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect2: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  contatoPro: {
    height: 40,
    marginTop: 11,
    marginRight: 1
  },
  textInput1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect1: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  contatoPro1: {
    height: 40,
    width: 375,
    marginTop: 11
  },
  textInput3: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect3: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  morada: {
    height: 40,
    marginTop: 11,
    marginLeft: 1
  },
  textInput4: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect4: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  nomeColumn: {
    marginRight: -1
  },
  nomeColumnFiller: {
    flex: 1
  },
  buttonMeds2: {
    width: 284,
    height: 84,
    marginBottom: 10,
    marginLeft: 46
  },
  btmBack2: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  registar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 26
  }
});

export default Login;

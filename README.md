**IPC: Mais Saúde**

O trabalho a ser desenvolvido tem como público-alvo a população idosa e tem como 
objetivo auxiliar na toma da medicação, lembretes de eventos e lembretes para a realização de 
atividade física. Isto tudo será feito com base num calendário mais simples do que aquele que 
existe no telemóvel e num sistema de lembretes/alarmes. 
Este público é caraterizado normalmente por ter algumas dificuldades no manuseamento
de dispositivos móveis, nomeadamente smartphones, devido a ser uma tecnologia recente e
muitos deles não terem o acompanhamento necessário para a entender. Além disso, as suas
incapacidades visuais/motoras acompanhadas de uma baixa taxa de literacia, nomeadamente em
Portugal, fazem com que tenhamos de modelar esta App da forma mais simples, objetiva e
“limpa” possível, permitindo assim a utilização desta por parte destes.

- 	Ana Ferreira al69136, anocasferreira2000@gmail.com;
- 	Edgar Pacheco al68765, edpacheco63@gmail.com;
- 	Francisco Pinto al68603, ffcpinto2@gmail.com;
- 	Francisco Loureiro al68737, franciscoloureiro29@gmail.com;
- 	Maria Magalhães al68609, rafaela.lpmagalhaes@gmail.com;
-   Nuno Guicho al68523, nunoguicho1@gmail.com;
- 	Rafael Moreira al68525, ra-fa-2000@hotmail.com;
- 	Rui Valente al68636, rui.valente99@gmail.com;



**Como compilar ?**

Abrir a pasta VSCode, abrir o terminal e correr os seguintes comandos:
`expo start `
ou
`npm start`

Caso não esteja instalado

`expo init `

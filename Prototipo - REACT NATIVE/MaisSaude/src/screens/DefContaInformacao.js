import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text,
  TextInput
} from "react-native";

function DefContaInformacao(props) {
  return (
    <View style={styles.container}>
      <View style={styles.profileStack}>
        <View style={styles.profile}>
          <ImageBackground
            source={require("../assets/images/image_XKVx..png")}
            resizeMode="stretch"
            style={styles.profilePic}
            imageStyle={styles.profilePic_imageStyle}
          >
            <View style={styles.buttonSettings}>
              <Image
                source={require("../assets/images/image_YOnL..png")}
                resizeMode="contain"
                style={styles.btsImg}
              ></Image>
            </View>
          </ImageBackground>
        </View>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.buttonBack}
        >
          <Image
            source={require("../assets/images/2961062-2001.png")}
            resizeMode="contain"
            style={styles.image1}
          ></Image>
        </TouchableOpacity>
      </View>
      <Text style={styles.conta2}>Conta</Text>
      <View style={styles.menu}>
        <View style={styles.background}>
          <View style={styles.inputs}>
            <View style={styles.nomeColumn}>
              <View style={styles.nome}>
                <TextInput
                  placeholder="Nome"
                  keyboardType="default"
                  maxLength={8}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  style={styles.textInput2}
                ></TextInput>
                <View style={styles.rect2}></View>
              </View>
              <View style={styles.contatoPro}>
                <TextInput
                  placeholder="Contato próprio"
                  keyboardType="phone-pad"
                  maxLength={9}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  dataDetector="phoneNumber"
                  style={styles.textInput1}
                ></TextInput>
                <View style={styles.rect1}></View>
              </View>
              <View style={styles.contatoPro1}>
                <TextInput
                  placeholder="Contato de emergência"
                  keyboardType="phone-pad"
                  maxLength={9}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  dataDetector="phoneNumber"
                  style={styles.textInput3}
                ></TextInput>
                <View style={styles.rect3}></View>
              </View>
              <View style={styles.morada}>
                <TextInput
                  placeholder="Morada"
                  keyboardType="default"
                  maxLength={25}
                  returnKeyType="go"
                  disableFullscreenUI={true}
                  dataDetector="address"
                  style={styles.textInput4}
                ></TextInput>
                <View style={styles.rect4}></View>
              </View>
            </View>
            <View style={styles.nomeColumnFiller}></View>
            <View style={styles.buttonMeds2Column}>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("Main")}
                style={styles.buttonMeds2}
              >
                <View style={styles.btmBack2}>
                  <Text style={styles.adicionar}>Alterar</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => props.navigation.navigate("Main")}
                style={styles.buttonMeds1}
              >
                <View style={styles.btmBack1}>
                  <Text style={styles.contato2}>📞 Contato</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  profile: {
    top: 20,
    left: 0,
    height: 170,
    position: "absolute",
    right: 0
  },
  profilePic: {
    width: 170,
    height: 170,
    alignSelf: "center"
  },
  profilePic_imageStyle: {},
  buttonSettings: {
    width: 40,
    height: 40,
    marginTop: 130,
    marginLeft: 118
  },
  btsImg: {
    height: 40,
    width: 40
  },
  buttonBack: {
    top: 0,
    left: 15,
    width: 50,
    height: 50,
    position: "absolute"
  },
  image1: {
    width: 50,
    height: 50
  },
  profileStack: {
    height: 190,
    marginTop: 40
  },
  conta2: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 10
  },
  menu: {
    height: 492,
    marginTop: 39
  },
  background: {
    height: 492,
    backgroundColor: "#E6E6E6",
    borderWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  inputs: {
    height: 400,
    marginTop: 40,
    alignSelf:"center"
  },
  nome: {
    height: 40,
    marginRight: 1
  },
  textInput2: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect2: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  contatoPro: {
    height: 40,
    marginTop: 11,
    marginRight: 1
  },
  textInput1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect1: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  contatoPro1: {
    height: 40,
    width: 375,
    marginTop: 11
  },
  textInput3: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect3: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  morada: {
    height: 40,
    marginTop: 11,
    marginLeft: 1
  },
  textInput4: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 300,
    height: 29,
    textAlign: "left",
    marginLeft: 38
  },
  rect4: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
    marginLeft: 38
  },
  nomeColumn: {
    marginRight: -1
  },
  nomeColumnFiller: {
    flex: 1
  },
  buttonMeds2: {
    width: 284,
    height: 84,
    marginBottom: 14
  },
  btmBack2: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  adicionar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 26
  },
  buttonMeds1: {
    width: 284,
    height: 84
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(0,0,0,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  contato2: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 26
  },
  buttonMeds2Column: {
    width: 284,
    marginBottom: 12,
    marginLeft: 46
  }
});

export default DefContaInformacao;

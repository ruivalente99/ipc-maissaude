import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

function WhiteBoard(props) {
  return (
    <View style={styles.background}>
  </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  background: {
    height: 492,
    backgroundColor: "#E6E6E6",
    borderWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  }
});

export default WhiteBoard;

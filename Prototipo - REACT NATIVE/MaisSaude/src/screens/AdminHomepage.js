import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text
} from "react-native";

function AdminHomePage(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <View style={styles.profile}>
        <ImageBackground
          source={require("../assets/images/image_XKVx..png")}
          resizeMode="stretch"
          style={styles.profilePic}
          imageStyle={styles.profilePic_imageStyle}
        >
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Def")}
            style={styles.button3}
          >
            <Image
              source={require("../assets/images/image_YOnL..png")}
              resizeMode="contain"
              style={styles.btsImg}
            ></Image>
          </TouchableOpacity>
        </ImageBackground>
      </View>
      <Text style={styles.olaAdminFerreira}>Olá, Admin Ferreira</Text>
      <View style={styles.buttonsMain}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("AprovarMed")}
          style={styles.buttonMeds}
        >
          <View style={styles.btmBack}>
            <Text style={styles.aprovarMedicos}>✔️ Aprovar Médicos</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("BloquearMed")}
          style={styles.button4}
        >
          <View style={styles.btcBack}>
            <Text style={styles.bloquearUtilizador}>
              🚫Bloquear Utilizadores
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("LoginEd")}
          style={styles.buttonEx}
        >
          <View style={styles.btexBack}>
            <Text style={styles.adicionarAdmin}>➕ Adicionar Admin</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  profile: {
    height: 170,
    marginTop: 60
  },
  profilePic: {
    width: 170,
    height: 170,
    alignSelf: "center"
  },
  profilePic_imageStyle: {},
  button3: {
    width: 40,
    height: 40,
    marginTop: 130,
    marginLeft: 118
  },
  btsImg: {
    height: 40,
    width: 40
  },
  olaAdminFerreira: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 10
  },
  buttonsMain: {
    height: 280,
    marginTop: 43
  },
  buttonMeds: {
    width: 284,
    height: 84,
    alignSelf: "center"
  },
  btmBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  aprovarMedicos: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 26
  },
  button4: {
    width: 284,
    height: 84,
    marginTop: 12,
    alignSelf: "center"
  },
  btcBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  bloquearUtilizador: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    height: 33,
    fontSize: 24,
    textAlign: "center",
    width: 284,
    marginTop: 26
  },
  buttonEx: {
    width: 284,
    height: 84,
    marginTop: 11,
    alignSelf: "center"
  },
  btexBack: {
    width: 284,
    height: 84,
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55,
    backgroundColor: "rgba(255,255,255,1)"
  },
  adicionarAdmin: {
    fontFamily: "roboto-regular",
    color: "rgba(0,0,0,1)",
    fontSize: 24,
    textAlign: "center",
    marginTop: 26
  }
});

export default AdminHomePage;

import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import MyDatePicker from "../components/myDatePicker";
function EditConsult(props) {
  return (
    <View style={styles.container}>
      <View style={styles.topBtn1}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.btnBack}
        >
          <Image
            source={require("../assets/images/2961062-2001.png")}
            resizeMode="contain"
            style={styles.backImg}
          ></Image>
        </TouchableOpacity>
        <View style={styles.btnBackFiller}></View>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("Main")}
          style={styles.btnHome}
        >
          <Image
            source={require("../assets/images/unknown.png")}
            resizeMode="contain"
            style={styles.homeImage}
          ></Image>
        </TouchableOpacity>
      </View>
      <View style={styles.btmId1}>
        <View style={styles.btmBack1}>
          <View style={styles.drEdgarStackStack}>
            <View style={styles.drEdgarStack}>
              <Text style={styles.drEdgar}>Dr. Edgar</Text>
              <View style={styles.btns}>
                <TouchableOpacity style={styles.button2}
                onPress={() => props.navigation.navigate("Consult")}>
                
                  <View style={styles.editImgStack}>
                    <Svg viewBox="0 0 45 45" style={styles.editImg}>
                      <Ellipse
                        stroke="rgba(230, 230, 230,1)"
                        strokeWidth={0}
                        fill="#1a5b92"
                        cx={23}
                        cy={23}
                        rx={23}
                        ry={23}
                      ></Ellipse>
                    </Svg>
                    <Text style={styles.editt}>✏️</Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.button2Filler}></View>
                <TouchableOpacity style={styles.button3}>
                  <View style={styles.delImgStack}>
                    <Svg viewBox="0 0 45 45" style={styles.delImg}>
                      <Ellipse
                        stroke="rgba(230, 230, 230,1)"
                        strokeWidth={0}
                        fill="rgba(230,57,70,1)"
                        cx={23}
                        cy={23}
                        rx={23}
                        ry={23}
                      ></Ellipse>
                    </Svg>
                    <Text style={styles.editt1}>🗑️</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <Svg viewBox="0 0 60 60" style={styles.btmImg1}>
              <Ellipse
                stroke="rgba(230, 230, 230,1)"
                strokeWidth={0}
                fill="rgba(230, 230, 230,1)"
                cx={30}
                cy={30}
                rx={30}
                ry={30}
              ></Ellipse>
            </Svg>
          </View>
          <Text style={styles.flexoes}>4/2 12:30</Text>
        </View>
      </View>
      <View style={styles.add}>
        <TouchableOpacity style={styles.button}
        onPress={() => props.navigation.navigate("Consult")}>
          <View style={styles.ellipseStack}>
            <Svg viewBox="0 0 60.5 60" style={styles.ellipse}>
              <Ellipse
                stroke="rgba(230, 230, 230,1)"
                strokeWidth={0}
                fill="rgba(42,157,143,1)"
                cx={30}
                cy={30}
                rx={30}
                ry={30}
              ></Ellipse>
            </Svg>
            <Text style={styles.plus}>+</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1a5b92"
  },
  topBtn1: {
    width: 345,
    height: 50,
    flexDirection: "row",
    marginTop: 40,
    marginLeft: 15
  },
  btnBack: {
    width: 50,
    height: 50
  },
  backImg: {
    width: 50,
    height: 50
  },
  btnBackFiller: {
    flex: 1,
    flexDirection: "row"
  },
  btnHome: {
    width: 50,
    height: 50
  },
  homeImage: {
    width: 50,
    height: 50
  },
  btmId1: {
    width: 284,
    height: 170,
    overflow: "visible",
    marginTop: 256,
    alignSelf: "center"
  },
  btmBack1: {
    width: 284,
    height: 170,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "rgba(0,0,0,1)",
    borderRadius: 55
  },
  drEdgar: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    left: 67,
    top: 8
  },
  btns: {
    height: 45,
    position: "absolute",
    width: 230,
    left: 0,
    top: 0,
    flexDirection: "row"
  },
  button2: {
    width: 45,
    height: 45,
    alignSelf: "center"
  },
  editImg: {
    top: 0,
    left: 0,
    width: 45,
    height: 45,
    position: "absolute"
  },
  editt: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    left: 6,
    top: 7
  },
  editImgStack: {
    width: 45,
    height: 45
  },
  button2Filler: {
    flex: 1,
    flexDirection: "row"
  },
  button3: {
    width: 45,
    height: 45,
    alignSelf: "center"
  },
  delImg: {
    top: 0,
    left: 0,
    width: 45,
    height: 45,
    position: "absolute"
  },
  editt1: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    left: 6,
    top: 7
  },
  delImgStack: {
    width: 45,
    height: 45
  },
  drEdgarStack: {
    top: 55,
    left: 0,
    width: 230,
    height: 45,
    position: "absolute"
  },
  btmImg1: {
    width: 60,
    height: 60,
    position: "absolute",
    top: 0,
    left: 85
  },
  drEdgarStackStack: {
    width: 230,
    height: 100,
    marginTop: 8,
    marginLeft: 27
  },
  flexoes: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    textAlign: "center",
    marginTop: 6
  },
  add: {
    height: 60,
    marginTop: -376
  },
  button: {
    width: 61,
    height: 60,
    alignSelf: "center"
  },
  ellipse: {
    top: 0,
    width: 61,
    height: 60,
    position: "absolute",
    left: 0
  },
  plus: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 45,
    alignSelf:"center"
  },
  ellipseStack: {
    width: 61,
    height: 60
  }
});

export default EditConsult;

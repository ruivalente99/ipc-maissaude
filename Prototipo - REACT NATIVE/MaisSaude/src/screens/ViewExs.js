import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function ViewExs(props) {
  return (
    <View style={styles.container}>
      <View style={styles.group}>
        <View style={styles.threepm}>
          <View style={styles.nineAmScroll1Row}>
            <View style={styles.nineAmScroll1}></View>
            <View style={styles.nIneamTime1Column}>
              <Text style={styles.nIneamTime1}>15:00</Text>
              <View style={styles.nineAmBTs1}>
                <View style={styles.btmId1}>
                  <View style={styles.btmBack3}>
                    <View style={styles.btmImg3Row}>
                      <Svg viewBox="0 0 60 60" style={styles.btmImg3}>
                        <Ellipse
                          stroke="rgba(230, 230, 230,1)"
                          strokeWidth={0}
                          fill="rgba(230, 230, 230,1)"
                          cx={30}
                          cy={30}
                          rx={30}
                          ry={30}
                        ></Ellipse>
                      </Svg>
                      <Text style={styles.btmText}>Flexões</Text>
                      <Text style={styles.btmTextDose3}>10reps</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.buttonStackRow}>
        <View style={styles.buttonStack}>
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={styles.button}
          ></TouchableOpacity>
          <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={styles.button1}
          >
            <Image
              source={require("../assets/images/2961062-200.png")}
              resizeMode="contain"
              style={styles.image1}
            ></Image>
          </TouchableOpacity>
        </View>
        <View style={styles.day}>
          <View style={styles.weeklyRow}>
            <Text style={styles.weekly}>Hoje</Text>
            <Text style={styles.n_day}>30/02</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  group: {
    width: 375,
    height: 143,
    marginTop: 335,
    alignSelf: "center"
  },
  threepm: {
    height: 143,
    marginLeft: 28,
    marginRight: 28
  },
  nineAmScroll1: {
    width: 1,
    height: 130,
    backgroundColor: "rgba(230, 230, 230,1)",
    marginTop: 3
  },
  nIneamTime1: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24
  },
  nineAmBTs1: {
    width: 284,
    height: 180,
    marginTop: 6
  },
  btmId1: {
    width: 284,
    height: 84,
    overflow: "visible"
  },
  btmBack3: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,0.75)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg3: {
    width: 60,
    height: 60
  },
  btmText: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 99,
    height: 29,
    marginLeft: 29,
    marginTop: 16
  },
  btmTextDose3: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 24,
    marginLeft: 1,
    marginTop: 16
  },
  btmImg3Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 6,
    marginLeft: 16,
    marginTop: 12
  },
  nIneamTime1Column: {
    width: 284,
    marginLeft: 9
  },
  nineAmScroll1Row: {
    height: 215,
    flexDirection: "row",
    marginRight: 26
  },
  button: {
    top: 0,
    left: 0,
    width: 45,
    height: 45,
    position: "absolute"
  },
  button1: {
    top: 0,
    left: 12,
    width: 50,
    height: 50,
    position: "absolute"
  },
  image1: {
    width: 50,
    height: 50
  },
  buttonStack: {
    width: 62,
    height: 50
  },
  day: {
    width: 113,
    height: 41,
    flexDirection: "row",
    marginLeft: 177
  },
  weekly: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34
  },
  n_day: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    marginLeft: 6,
    marginTop: 20
  },
  weeklyRow: {
    height: 41,
    flexDirection: "row",
    flex: 1
  },
  buttonStackRow: {
    height: 50,
    flexDirection: "row",
    marginTop: -438,
    marginLeft: 3,
    marginRight: 20
  }
});

export default ViewExs;

import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import MaterialFixedLabelTextbox from "../components/MaterialFixedLabelTextbox";
import MaterialFixedLabelTextbox2 from "../components/MaterialFixedLabelTextbox2";
import MaterialFixedLabelTextbox1 from "../components/MaterialFixedLabelTextbox1";
import MaterialFixedLabelTextbox3 from "../components/MaterialFixedLabelTextbox3";

function Login(props) {
  return (
    <View style={styles.container}>
      <View style={styles.buttonStack}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button}
        >
          <Image
            source={require("../assets/images/2961062-200.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
        </TouchableOpacity>
        <View style={styles.fotoLogin}>
          <View style={styles.foto}>
            <Image
              source={require("../assets/images/image_XKVx..png")}
              resizeMode="contain"
              style={styles.image2}
            ></Image>
          </View>
        </View>
      </View>
      <View style={styles.painelDefinicoes}>
        <View style={styles.definicoes}>
          <View style={styles.nomeTextBox}>
            <View style={styles.inputNomeStack}>
              <MaterialFixedLabelTextbox
                style={styles.inputNome}
              ></MaterialFixedLabelTextbox>
              <View style={styles.nineAmScroll1}></View>
            </View>
          </View>
          <View style={styles.campoMail}>
            <View style={styles.inputMailStack}>
              <MaterialFixedLabelTextbox2
                style={styles.inputMail}
              ></MaterialFixedLabelTextbox2>
              <View style={styles.rect3}></View>
            </View>
          </View>
          <View style={styles.campoIdade}>
            <View style={styles.inputIdadeStack}>
              <MaterialFixedLabelTextbox1
                style={styles.inputIdade}
              ></MaterialFixedLabelTextbox1>
              <View style={styles.rect7}></View>
            </View>
          </View>
          <View style={styles.campoPass}>
            <View style={styles.rect5Row}>
              <View style={styles.rect5}></View>
              <MaterialFixedLabelTextbox3
                style={styles.inputPass}
              ></MaterialFixedLabelTextbox3>
            </View>
          </View>
          <View style={styles.bTmID1}>
            <View style={styles.rect1}>
              <Text style={styles.registar}>Registar</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.logo}>
        <Image
          source={require("../assets/images/image_iTiA..png")}
          resizeMode="contain"
          style={styles.logoImg}
        ></Image>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  button: {
    top: 0,
    left: 9,
    width: 46,
    height: 45,
    position: "absolute"
  },
  image: {
    width: 46,
    height: 45
  },
  fotoLogin: {
    top: 19,
    left: 0,
    height: 201,
    position: "absolute",
    right: 0
  },
  foto: {
    width: 200,
    height: 201,
    alignSelf: "center"
  },
  image2: {
    height: 200,
    width: 200
  },
  buttonStack: {
    height: 220,
    marginTop: 40
  },
  painelDefinicoes: {
    height: 434
  },
  definicoes: {
    width: 375,
    height: 434,
    alignSelf: "center"
  },
  nomeTextBox: {
    width: 300,
    height: 54,
    marginTop: 34,
    marginLeft: 29
  },
  inputNome: {
    height: 43,
    width: 300,
    position: "absolute",
    left: 0,
    top: 0
  },
  nineAmScroll1: {
    top: 0,
    left: 0,
    width: 1,
    height: 35,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)"
  },
  inputNomeStack: {
    width: 300,
    height: 43,
    marginTop: 16
  },
  campoMail: {
    width: 300,
    height: 54,
    marginTop: 19,
    marginLeft: 29
  },
  inputMail: {
    height: 43,
    width: 300,
    position: "absolute",
    left: 0,
    top: 0
  },
  rect3: {
    top: 5,
    left: 0,
    width: 1,
    height: 35,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)"
  },
  inputMailStack: {
    width: 300,
    height: 43,
    marginTop: 11
  },
  campoIdade: {
    width: 300,
    height: 54,
    marginTop: 19,
    marginLeft: 29
  },
  inputIdade: {
    height: 43,
    width: 300,
    position: "absolute",
    left: 0,
    top: 0
  },
  rect7: {
    top: 5,
    left: 0,
    width: 1,
    height: 35,
    position: "absolute",
    backgroundColor: "rgba(0,0,0,1)"
  },
  inputIdadeStack: {
    width: 300,
    height: 43,
    marginTop: 11
  },
  campoPass: {
    width: 300,
    height: 54,
    flexDirection: "row",
    marginTop: 19,
    marginLeft: 29
  },
  rect5: {
    width: 1,
    height: 35,
    backgroundColor: "rgba(0,0,0,1)",
    marginTop: 5
  },
  inputPass: {
    height: 43,
    width: 300
  },
  rect5Row: {
    height: 43,
    flexDirection: "row",
    flex: 1,
    marginRight: -1,
    marginTop: 11
  },
  bTmID1: {
    width: 284,
    height: 84,
    marginTop: 27,
    marginLeft: 46
  },
  rect1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderRadius: 55
  },
  registar: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    width: 90,
    height: 29,
    marginTop: 28,
    marginLeft: 97
  },
  logo: {
    height: 60,
    marginTop: 18
  },
  logoImg: {
    height: 60,
    width: 60,
    alignSelf: "center"
  }
});

export default Login;

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView
} from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function ViewMeds(props) {
  return (
    <View style={styles.container}>
      <View style={styles.buttonStack}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button}
        >
          <Image
            source={require("../assets/images/2961062-200.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
        </TouchableOpacity>
        <View style={styles.dayNdate}>
          <View style={styles.day}>
            <View style={styles.weeklyRow}>
              <Text style={styles.weekly}>Hoje</Text>
              <Text style={styles.n_day}>30/02</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.buttonStackFiller}>
        <View style={styles.meds}>
          <View style={styles.nineAm_Scroll}>
            <ScrollView
              horizontal={false}
              contentContainerStyle={styles.nineAm_Scroll_contentContainerStyle}
            >
              <View style={styles.nineAM_ScrollRow}>
                <View style={styles.nineAM_Scroll}></View>
                <View style={styles.nineAM_BTsStackStack}>
                  <View style={styles.nineAM_BTsStack}>
                    <View style={styles.nineAM_BTs}>
                      <View style={styles.btm_ID_0}>
                        <View style={styles.btmBack}>
                          <View style={styles.btmImgRow}>
                            <Svg viewBox="0 0 60 60" style={styles.btmImg}>
                              <Ellipse
                                stroke="rgba(230, 230, 230,1)"
                                strokeWidth={0}
                                fill="rgba(230, 230, 230,1)"
                                cx={30}
                                cy={30}
                                rx={30}
                                ry={30}
                              ></Ellipse>
                            </Svg>
                            <Text style={styles.btmTextName}>Xanax</Text>
                            <Text style={styles.btmTextDose}>1mg</Text>
                          </View>
                        </View>
                      </View>
                      <View style={styles.btm_ID_1}>
                        <View style={styles.btmBack1}>
                          <View style={styles.btmImg1Row}>
                            <Svg viewBox="0 0 60 60" style={styles.btmImg1}>
                              <Ellipse
                                stroke="rgba(230, 230, 230,1)"
                                strokeWidth={0}
                                fill="rgba(230, 230, 230,1)"
                                cx={30}
                                cy={30}
                                rx={30}
                                ry={30}
                              ></Ellipse>
                            </Svg>
                            <Text style={styles.btmTextName1}>Benuron</Text>
                            <Text style={styles.btmTextDose1}>50mg</Text>
                          </View>
                        </View>
                      </View>
                      <View style={styles.btm_ID_2}>
                        <View style={styles.btmBack2}>
                          <View style={styles.btmImg2Row}>
                            <Svg viewBox="0 0 60 60" style={styles.btmImg2}>
                              <Ellipse
                                stroke="rgba(230, 230, 230,1)"
                                strokeWidth={0}
                                fill="rgba(230, 230, 230,1)"
                                cx={30}
                                cy={30}
                                rx={30}
                                ry={30}
                              ></Ellipse>
                            </Svg>
                            <Text style={styles.tylnol}>Tylnol</Text>
                            <Text style={styles.btmTextDose2}>50mg</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                    <View style={styles.nINEAM_Back}></View>
                  </View>
                  <Text style={styles.nINEAM_time}>9:00</Text>
                </View>
              </View>
            </ScrollView>
          </View>
          <View style={styles.threePM_NoScroll}>
            <View style={styles.nineAmScroll1Row}>
              <View style={styles.nineAmScroll1}></View>
              <View style={styles.nIneamTime1StackStack}>
                <View style={styles.nIneamTime1Stack}>
                  <Text style={styles.nIneamTime1}>15:00</Text>
                  <View style={styles.nIneamBack1}></View>
                </View>
                <View style={styles.nineAmBTs1}>
                  <View style={styles.btmId1}>
                    <View style={styles.btmBack3}>
                      <View style={styles.btmImg3Row}>
                        <Svg viewBox="0 0 60 60" style={styles.btmImg3}>
                          <Ellipse
                            stroke="rgba(230, 230, 230,1)"
                            strokeWidth={0}
                            fill="rgba(230, 230, 230,1)"
                            cx={30}
                            cy={30}
                            rx={30}
                            ry={30}
                          ></Ellipse>
                        </Svg>
                        <Text style={styles.btmTextName2}>Xanax</Text>
                        <Text style={styles.btmTextDose3}>1mg</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  button: {
    top: 0,
    left: 15,
    width: 50,
    height: 50,
    position: "absolute"
  },
  image: {
    width: 50,
    height: 50
  },
  dayNdate: {
    top: 0,
    left: 0,
    height: 65,
    position: "absolute",
    right: 0
  },
  day: {
    width: 113,
    height: 41,
    flexDirection: "row",
    marginLeft: 247
  },
  weekly: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34
  },
  n_day: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 14,
    marginLeft: 6,
    marginTop: 20
  },
  weeklyRow: {
    height: 41,
    flexDirection: "row",
    flex: 1
  },
  buttonStack: {
    height: 65,
    marginTop: 40
  },
  meds: {
    height: 400
  },
  nineAm_Scroll: {
    height: 223,
    width: 320,
    alignSelf: "center"
  },
  nineAm_Scroll_contentContainerStyle: {
    height: 323,
    width: 320,
    flexDirection: "row"
  },
  nineAM_Scroll: {
    width: 1,
    height: 320,
    backgroundColor: "rgba(230, 230, 230,1)",
    marginTop: 3
  },
  nineAM_BTs: {
    top: 32,
    left: 9,
    width: 284,
    height: 180,
    position: "absolute"
  },
  btm_ID_0: {
    width: 284,
    height: 84,
    overflow: "visible"
  },
  btmBack: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "rgba(0,0,0,1)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg: {
    width: 60,
    height: 60
  },
  btmTextName: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    marginLeft: 32,
    marginTop: 16
  },
  btmTextDose: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 24,
    marginLeft: 32,
    marginTop: 16
  },
  btmImgRow: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 29,
    marginLeft: 16,
    marginTop: 12
  },
  btm_ID_1: {
    width: 284,
    height: 84,
    overflow: "visible",
    marginTop: 12
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "rgba(0,0,0,1)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg1: {
    width: 60,
    height: 60
  },
  btmTextName1: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    marginLeft: 21,
    marginTop: 16
  },
  btmTextDose1: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 24,
    marginLeft: 21,
    marginTop: 16
  },
  btmImg1Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 15,
    marginLeft: 16,
    marginTop: 12
  },
  btm_ID_2: {
    width: 284,
    height: 84,
    overflow: "visible",
    marginTop: 11
  },
  btmBack2: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,1)",
    borderWidth: 0,
    borderColor: "rgba(0,0,0,1)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg2: {
    width: 60,
    height: 60
  },
  tylnol: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    marginLeft: 34,
    marginTop: 16
  },
  btmTextDose2: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 24,
    marginLeft: 33,
    marginTop: 16
  },
  btmImg2Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 15,
    marginLeft: 16,
    marginTop: 12
  },
  nINEAM_Back: {
    top: 0,
    left: 0,
    position: "absolute",
    backgroundColor: "#E6E6E6",
    height: 0,
    width: 0,
    opacity: 0
  },
  nineAM_BTsStack: {
    top: 3,
    left: 0,
    width: 364,
    height: 320,
    position: "absolute"
  },
  nINEAM_time: {
    top: 0,
    left: 9,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24
  },
  nineAM_BTsStackStack: {
    width: 364,
    height: 323
  },
  nineAM_ScrollRow: {
    height: 323,
    flexDirection: "row",
    flex: 1,
    marginRight: -45
  },
  threePM_NoScroll: {
    height: 223,
    width: 320,
    flexDirection: "row",
    marginTop: 34,
    alignSelf: "center"
  },
  nineAmScroll1: {
    width: 1,
    height: 130,
    backgroundColor: "rgba(230, 230, 230,1)",
    marginTop: 3
  },
  nIneamTime1: {
    top: 0,
    left: 9,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24
  },
  nIneamBack1: {
    top: 3,
    left: 0,
    width: 364,
    height: 320,
    position: "absolute",
    opacity: 0.1
  },
  nIneamTime1Stack: {
    top: 0,
    left: 0,
    width: 364,
    height: 323,
    position: "absolute"
  },
  nineAmBTs1: {
    top: 35,
    left: 9,
    width: 284,
    height: 180,
    position: "absolute"
  },
  btmId1: {
    width: 284,
    height: 84,
    overflow: "visible"
  },
  btmBack3: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(255,255,255,0.75)",
    borderRadius: 55,
    flexDirection: "row"
  },
  btmImg3: {
    width: 60,
    height: 60
  },
  btmTextName2: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    marginLeft: 32,
    marginTop: 16
  },
  btmTextDose3: {
    fontFamily: "roboto-regular",
    color: "rgba(230,57,70,1)",
    fontSize: 24,
    marginLeft: 32,
    marginTop: 16
  },
  btmImg3Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 29,
    marginLeft: 16,
    marginTop: 12
  },
  nIneamTime1StackStack: {
    width: 364,
    height: 323
  },
  nineAmScroll1Row: {
    height: 323,
    flexDirection: "row",
    flex: 1,
    marginRight: -45
  },
  buttonStackFiller: {
    flex: 1,
    justifyContent: "center"
  }
});

export default ViewMeds;

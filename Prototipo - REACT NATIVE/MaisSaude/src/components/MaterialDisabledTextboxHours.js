import React, { Component } from "react";
import { StyleSheet, View, TextInput } from "react-native";

function MaterialDisabledTextboxHours(props) {
  return (
    <View style={[styles.container, props.style]}>
      <TextInput
        placeholder="Horas"
        editable={false}
        placeholderTextColor="rgba(255,255,255,1)"
        style={styles.inputStyle}
      ></TextInput>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 0,
    borderColor: "#000000",
    borderBottomWidth: 1
  },
  inputStyle: {
    color: "#000",
    fontSize: 16,
    flex: 1,
    lineHeight: 16,
    height: 42,
    width: 358,
    marginTop: 1,
    marginLeft: 1
  }
});

export default MaterialDisabledTextboxHours;

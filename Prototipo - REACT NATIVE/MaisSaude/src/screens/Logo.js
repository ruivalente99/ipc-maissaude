import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";

function Logo(props) {
  return (
    <View style={styles.container}>
      <View style={styles.centrar}>
        <Image
          source={require("../assets/images/image_DeXK..png")}
          resizeMode="contain"
          style={styles.image}
        ></Image>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  centrar: {
    height: 200,
    marginTop: 306
  },
  image: {
    height: 200,
    width: 200,
    alignSelf: "center"
  }
});

export default Logo;

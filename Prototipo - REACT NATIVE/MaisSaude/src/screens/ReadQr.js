import React, { Component } from "react";
import {
  Alert,
  StyleSheet,
  View,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground
} from "react-native";
import {RNCamera}  from 'react-native-camera';
function ReadQr(props) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Text style={styles.lerQr}>Ler QR</Text>
      <View style={styles.menu}>
        <View style={styles.background}>
          <View style={styles.rectColumn}>
            <View style={styles.rect}>
                  {/*  <RNCamera
                  ref={ref => {
                    this.camera = ref;
                  }}
                  style={styles.preview}
                  type={RNCamera.Constants.Type.back}
                  flashMode={RNCamera.Constants.FlashMode.on}
                  androidCameraPermissionOptions={{
                    title: 'Permission to use camera',
                    message: 'We need your permission to use your camera',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                  }}
                  androidRecordAudioPermissionOptions={{
                    title: 'Permission to use audio recording',
                    message: 'We need your permission to use your audio',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                  }}
                  onGoogleVisionBarcodesDetected={({ barcodes }) => {
                    console.log(barcodes);
                  }}
                /> */}
            </View>
            <View style={styles.group}>
              <TextInput
                placeholder="Inserir código manualmente"
                keyboardType="numeric"
                maxLength={8}
                returnKeyType="go"
                disableFullscreenUI={true}
                style={styles.textInput}
              ></TextInput>
              <View style={styles.rect2}></View>
            </View>
          </View>
          <View style={styles.rectColumnFiller}></View>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("DocControl")}
            style={styles.buttonConsult1}
          >
            <View style={styles.btcBack1}>
              <Text style={styles.sessaoRemota}>Sessão remota</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.profile1Stack}>
        <View style={styles.profile1}>
          <ImageBackground
            source={require("../assets/images/image_ZE78..png")}
            resizeMode="contain"
            style={styles.profilePic}
            imageStyle={styles.profilePic_imageStyle}
          >
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Def")}
              style={styles.btSettings}
            >
              <Image
                source={require("../assets/images/image_YOnL..png")}
                resizeMode="contain"
                style={styles.btsImg1}
              ></Image>
            </TouchableOpacity>
          </ImageBackground>
        </View>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button2}
        >
          <Image
            source={require("../assets/images/2961062-2001.png")}
            resizeMode="contain"
            style={styles.image1}
          ></Image>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  lerQr: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 34,
    textAlign: "center",
    marginTop: 240,
    alignSelf: "center"
  },
  menu: {
    height: 492,
    marginTop: 39
  },
  background: {
    height: 492,
    backgroundColor: "#E6E6E6",
    borderWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  rect: {
    backgroundColor: "#fff",
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  group: {
    height: 40,
    marginTop: 20,
    alignSelf: "center"
  },
  textInput: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    textAlign: "center",
  },
  rect2: {
    height: 1,
    backgroundColor: "rgba(0,0,0,1)",
    width: 300,
    marginTop: 2,
  },
  rectColumn: {
    marginTop: 20
  },
  rectColumnFiller: {
    flex: 1
  },
  buttonConsult1: {
    width: 284,
    height: 84,
    marginBottom: 48,
    alignSelf: "center"
  },
  btcBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(42,157,143,1)",
    borderWidth: 0,
    borderColor: "#000000",
    borderRadius: 55
  },
  sessaoRemota: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    textAlign: "center",
    marginTop: 22
  },
  profile1: {
    top: 20,
    left: 0,
    height: 170,
    position: "absolute",
    right: 0
  },
  profilePic: {
    height: 170,
    width: 170,
    alignSelf: "center"
  },
  profilePic_imageStyle: {},
  btSettings: {
    width: 40,
    height: 40,
    marginTop: 130,
    marginLeft: 118
  },
  btsImg1: {
    height: 40,
    width: 40
  },
  button2: {
    top: 0,
    left: 15,
    width: 50,
    height: 50,
    position: "absolute"
  },
  image1: {
    width: 50,
    height: 50
  },
  profile1Stack: {
    height: 190,
    marginTop: -772
  }
});

export default ReadQr;

import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import CupertinoRadio from "../components/CupertinoRadio";

function DefLinguagem1(props) {
  return (
    <View style={styles.container}>
      <View style={styles.buttonStack}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button}
        >
          <Image
            source={require("../assets/images/2961062-200.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
        </TouchableOpacity>
        <View style={styles.adminImagemLinguagem}>
          <View style={styles.imagemPrincipal1}>
            <Image
              source={require("../assets/images/image_XKVx..png")}
              resizeMode="contain"
              style={styles.image2}
            ></Image>
            <Text style={styles.linguagem2}>Linguagem</Text>
          </View>
        </View>
      </View>
      <View style={styles.painelLinguagem}>
        <View style={styles.definicoes}>
          <View style={styles.retangulo}>
            <View style={styles.linguagens}>
              <View style={styles.pT}>
                <View style={styles.portuguesStack}>
                  <Text style={styles.portugues}>Português</Text>
                  <CupertinoRadio
                    style={styles.cupertinoRadio6}
                  ></CupertinoRadio>
                </View>
              </View>
              <View style={styles.eN}>
                <View style={styles.cupertinoRadioRow}>
                  <CupertinoRadio
                    style={styles.cupertinoRadio}
                  ></CupertinoRadio>
                  <Text style={styles.english}>English</Text>
                </View>
              </View>
              <View style={styles.fR}>
                <View style={styles.cupertinoRadio5Row}>
                  <CupertinoRadio
                    style={styles.cupertinoRadio5}
                  ></CupertinoRadio>
                  <Text style={styles.francais}>Français</Text>
                </View>
              </View>
              <View style={styles.dE}>
                <View style={styles.cupertinoRadio4Row}>
                  <CupertinoRadio
                    style={styles.cupertinoRadio4}
                  ></CupertinoRadio>
                  <Text style={styles.deutsch}>Deutsch</Text>
                </View>
              </View>
              <View style={styles.rU}>
                <View style={styles.cupertinoRadio3Row}>
                  <CupertinoRadio
                    style={styles.cupertinoRadio3}
                  ></CupertinoRadio>
                  <Text style={styles.rumantsch}>Rumantsch</Text>
                </View>
              </View>
              <View style={styles.eS}>
                <View style={styles.cupertinoRadio2Row}>
                  <CupertinoRadio
                    style={styles.cupertinoRadio2}
                  ></CupertinoRadio>
                  <Text style={styles.espanol}>Español</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  button: {
    top: 0,
    left: 9,
    width: 46,
    height: 45,
    position: "absolute"
  },
  image: {
    width: 46,
    height: 45
  },
  adminImagemLinguagem: {
    top: 19,
    left: 0,
    height: 257,
    position: "absolute",
    right: 0
  },
  imagemPrincipal1: {
    width: 200,
    height: 257,
    alignSelf: "center"
  },
  image2: {
    height: 200,
    width: 200
  },
  linguagem2: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    flex: 1,
    marginBottom: -37,
    marginTop: 20,
    alignSelf: "center"
  },
  buttonStack: {
    height: 276,
    marginTop: 40
  },
  painelLinguagem: {
    height: 481,
    marginTop: 15
  },
  definicoes: {
    width: 375,
    height: 481,
    alignSelf: "center"
  },
  retangulo: {
    height: 481,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 39,
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0,
    borderTopLeftRadius: 55,
    borderTopRightRadius: 55
  },
  linguagens: {
    width: 40,
    height: 40,
    marginTop: 104,
    marginLeft: 32
  },
  pT: {
    width: 109,
    height: 40
  },
  portugues: {
    top: 18,
    left: 55,
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 20
  },
  cupertinoRadio6: {
    height: 60,
    width: 60,
    position: "absolute",
    left: 0,
    top: 0
  },
  portuguesStack: {
    width: 147,
    height: 60,
    marginTop: -10,
    marginLeft: -11
  },
  eN: {
    width: 90,
    height: 40,
    flexDirection: "row",
    marginTop: 39
  },
  cupertinoRadio: {
    height: 40,
    width: 40
  },
  english: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 20,
    marginLeft: 9,
    marginTop: 8
  },
  cupertinoRadioRow: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: -24
  },
  fR: {
    width: 98,
    height: 40,
    flexDirection: "row"
  },
  cupertinoRadio5: {
    height: 40,
    width: 40
  },
  francais: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 20,
    marginLeft: 9,
    marginTop: 8
  },
  cupertinoRadio5Row: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: -27
  },
  dE: {
    width: 96,
    height: 40,
    flexDirection: "row"
  },
  cupertinoRadio4: {
    height: 40,
    width: 40
  },
  deutsch: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 20,
    marginLeft: 8,
    marginTop: 8
  },
  cupertinoRadio4Row: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: -25
  },
  rU: {
    width: 116,
    height: 40,
    flexDirection: "row"
  },
  cupertinoRadio3: {
    height: 40,
    width: 40
  },
  rumantsch: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 20,
    marginLeft: 8,
    marginTop: 8
  },
  cupertinoRadio3Row: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: -33
  },
  eS: {
    width: 95,
    height: 40,
    flexDirection: "row"
  },
  cupertinoRadio2: {
    height: 40,
    width: 40
  },
  espanol: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 20,
    marginLeft: 7,
    marginTop: 8
  },
  cupertinoRadio2Row: {
    height: 40,
    flexDirection: "row",
    flex: 1,
    marginRight: -24
  }
});

export default DefLinguagem1;

import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";

function Def(props) {
  return (
    <View style={styles.container}>
      <View style={styles.buttonStack}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.button}
        >
          <Image
            source={require("../assets/images/2961062-200.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
        </TouchableOpacity>
        <View style={styles.adminImagemDef}>
          <View style={styles.imagemPrincipal1}>
            <Image
              source={require("../assets/images/image_XKVx..png")}
              resizeMode="contain"
              style={styles.image2}
            ></Image>
            <Text style={styles.definicoesText}>Definições</Text>
          </View>
        </View>
      </View>
      <View style={styles.painelDef}>
        <View style={styles.definicoes}>
          <View style={styles.retangulo}>
            <View style={styles.btmId1}>
              <View style={styles.btmBack1}>
                <View style={styles.btmImg1Row}>
                  <Svg viewBox="0 0 60 60" style={styles.btmImg1}>
                    <Ellipse
                      stroke="rgba(230, 230, 230,1)"
                      strokeWidth={0}
                      fill="rgba(230, 230, 230,1)"
                      cx={30}
                      cy={30}
                      rx={30}
                      ry={30}
                    ></Ellipse>
                  </Svg>
                  <Text style={styles.linguagem}>Linguagem</Text>
                </View>
              </View>
            </View>
            <View style={styles.btmId2}>
              <View style={styles.rect}>
                <View style={styles.ellipseRow}>
                  <Svg viewBox="0 0 60 60" style={styles.ellipse}>
                    <Ellipse
                      stroke="rgba(230, 230, 230,1)"
                      strokeWidth={0}
                      fill="rgba(230, 230, 230,1)"
                      cx={30}
                      cy={30}
                      rx={30}
                      ry={30}
                    ></Ellipse>
                  </Svg>
                  <Text style={styles.conta}>Conta</Text>
                </View>
              </View>
            </View>
            <View style={styles.btmId3}>
              <View style={styles.rect2Stack}>
                <View style={styles.rect2}>
                  <Svg viewBox="0 0 60 60" style={styles.ellipse2}>
                    <Ellipse
                      stroke="rgba(230, 230, 230,1)"
                      strokeWidth={0}
                      fill="rgba(230, 230, 230,1)"
                      cx={30}
                      cy={30}
                      rx={30}
                      ry={30}
                    ></Ellipse>
                  </Svg>
                </View>
                <Text style={styles.terminarSessao}>Terminar Sessão</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(26,91,146,1)"
  },
  button: {
    top: 0,
    left: 9,
    width: 46,
    height: 45,
    position: "absolute"
  },
  image: {
    width: 46,
    height: 45
  },
  adminImagemDef: {
    top: 19,
    left: 0,
    height: 259,
    position: "absolute",
    right: 0
  },
  imagemPrincipal1: {
    width: 200,
    height: 259,
    alignSelf: "center"
  },
  image2: {
    height: 200,
    width: 200
  },
  definicoesText: {
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    flex: 1,
    marginBottom: -37,
    marginTop: 20,
    alignSelf: "center"
  },
  buttonStack: {
    height: 278,
    marginTop: 40
  },
  painelDef: {
    height: 481,
    marginTop: 13
  },
  definicoes: {
    width: 375,
    height: 481,
    justifyContent: "center",
    alignSelf: "center"
  },
  retangulo: {
    width: 375,
    height: 481,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 39,
    shadowColor: "rgba(0,0,0,1)",
    shadowOffset: {
      width: 3,
      height: 3
    },
    elevation: 5,
    shadowOpacity: 1,
    shadowRadius: 0
  },
  btmId1: {
    width: 284,
    height: 84,
    overflow: "visible",
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 64,
    marginLeft: 46
  },
  btmBack1: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(191,191,191,1)",
    borderRadius: 55,
    flexDirection: "row",
    alignSelf: "center"
  },
  btmImg1: {
    width: 60,
    height: 60
  },
  linguagem: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 143,
    height: 29,
    marginLeft: 15,
    marginTop: 16
  },
  btmImg1Row: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 50,
    marginLeft: 16,
    marginTop: 12
  },
  btmId2: {
    width: 284,
    height: 84,
    overflow: "visible",
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 36,
    marginLeft: 46
  },
  rect: {
    width: 284,
    height: 84,
    backgroundColor: "rgba(191,191,191,1)",
    borderRadius: 55,
    flexDirection: "row",
    alignSelf: "center"
  },
  ellipse: {
    width: 60,
    height: 60
  },
  conta: {
    fontFamily: "roboto-regular",
    color: "#121212",
    fontSize: 24,
    width: 143,
    height: 29,
    marginLeft: 15,
    marginTop: 16
  },
  ellipseRow: {
    height: 60,
    flexDirection: "row",
    flex: 1,
    marginRight: 50,
    marginLeft: 16,
    marginTop: 12
  },
  btmId3: {
    width: 284,
    height: 84,
    overflow: "visible",
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 36,
    marginLeft: 46
  },
  rect2: {
    width: 284,
    height: 84,
    position: "absolute",
    backgroundColor: "rgba(230,57,70,1)",
    borderRadius: 55,
    top: 0,
    left: 0
  },
  ellipse2: {
    width: 60,
    height: 60,
    marginTop: 12,
    marginLeft: 16
  },
  terminarSessao: {
    position: "absolute",
    fontFamily: "roboto-regular",
    color: "rgba(255,255,255,1)",
    fontSize: 24,
    left: 91,
    top: 28,
    width: 199,
    height: 29
  },
  rect2Stack: {
    width: 290,
    height: 84
  }
});

export default Def;
